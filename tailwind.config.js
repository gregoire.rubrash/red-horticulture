/** @type {import('tailwindcss').Config} config */

const config = {
  content: ['./index.php', './app/**/*.php', './resources/**/*.{php,vue,js}'],
  theme: {
    extend: {
      colors: {
        red: '#D41A1A',
        black: '#141414',
        yellow: '#F4B400',
      },
      gradientColorStops: {
        red: '#D41A1A',
      },
      fontFamily: {
        sans: 'General Sans',
      },
      zIndex: {
        1: '1',
        2: '2',
        3: '3',
        4: '4',
        5: '5',
        '-1': '-1',
        '-2': '-2',
        '-3': '-3',
        '-4': '-4',
        '-5': '-5',
      },
      animation: {
        fade: 'fade 600ms ease-in-out',
        marquee: 'scroll-x 60s linear infinite',
        'marquee-reverse': 'scroll-x 60s linear -300ms infinite reverse',
      },
      keyframes: {
        fade: {
          '0%': { transform: 'translateX(-1em)', opacity: '0' },
          '100%': { transform: 'translateX(0)' },
        },
        'scroll-x': {
          '0%': { transform: 'translateX(var(--scroll-start, 0))' },
          '100%': { transform: 'translateX(var(--scroll-end, -100%))' },
        },
      },
    },
  },
  plugins: [],
};

export default config;
