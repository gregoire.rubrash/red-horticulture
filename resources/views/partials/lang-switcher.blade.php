<nav class="relative flex" role="navigation" data-module-lang>
  <button class="aspect-square h-9 w-auto" data-lang="button">
    <img class="block aspect-square h-full w-full" title="{{ pll_current_language() }}"
      src="{{ asset('images/svg/flags/' . pll_current_language() . '.svg') }}" alt="{{ pll_current_language() }}" />
  </button>

  <ul class="invisible absolute -left-2 bottom-[calc(-100%-1rem-4px)] w-max bg-white opacity-0 shadow-md"
    aria-hidden="true" data-lang="list">
    @foreach (pll_the_languages(['dropdown' => 1, 'hide_current' => 1, 'raw' => 1]) as $lang)
      <li>
        <a class="flex items-center gap-2 px-4 py-2 hover:text-red" href="<?php echo $lang['url']; ?>">
          <img class="block aspect-square h-6 w-6" src="{{ asset('images/svg/flags/' . $lang['slug'] . '.svg') }}"
            alt="{{ $lang['slug'] }}" />
          <span class="capitalize">- {{ $lang['name'] }}</span>
        </a>
      </li>
    @endforeach
  </ul>
</nav>
