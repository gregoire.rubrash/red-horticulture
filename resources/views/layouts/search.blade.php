<div class="mb-12">
  @includeIf('forms.search', [
      'placeholder' => pll__('Rechercher', 'rubrash'),
  ])
</div>

<div class="grid gap-6 px-7 md:grid-cols-2 lg:grid-cols-3 lg:pl-[10%] lg:pr-[2%]">
  @forelse ($posts as $post)
    <x-card-search :post="$post" />
  @empty
    <p>Aucun résultat</p>
  @endforelse
</div>
