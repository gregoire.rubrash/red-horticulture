<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=5">
  <meta name="creator" content="Grégoire Ciles">
  <meta name="robots" content="all">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <meta name="apple-mobile-web-app-title" content="{{ get_bloginfo('name') }}">
  <meta name="application-name" content="{{ get_bloginfo('name') }}">
  <meta name="mobile-web-app-capable" content="yes">
  <meta property="og:image" content="@asset('images/og.jpg')">
  <meta property="og:image:width" content="2400">
  <meta property="og:image:height" content="1260">
  <link rel="shortcut icon" sizes="16x16" href="@asset('images/favicon-16x16.png')">
  <link rel="icon" sizes="any" href="@asset('images/favicon.ico')">
  <link rel="apple-touch-icon" href="@asset('images/apple-touch-icon.png')">
  <link rel="manifest" href="@asset('images/site.webmanifest')">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <?php wp_head(); ?>
  @stack('scripts')
</head>

<body>
  <?php wp_body_open(); ?>
  <?php do_action('get_header'); ?>

  @include('sections.skiplinks')
  @include('sections.header')

  <main id="main" class="main relative">
    <x-scroll-indicator />

    <div class="blocks">
      @yield('content')
    </div>
  </main>

  @include('sections.footer')

  <?php do_action('get_footer'); ?>
  <?php wp_footer(); ?>
  @stack('styles')
</body>

</html>
