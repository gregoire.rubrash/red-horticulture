@php
  global $wp_query;
  $query_vars = $wp_query->query_vars;
@endphp

@if (!empty($highlightedPost))
  <x-content-full-img :title="$highlightedPost->title" :content="$highlightedPost->excerpt" :image="$highlightedPost->thumbnail" :alignRight="true" :link="$highlightedPost->link"
    :isArchive="true" :hasBg="true" />
@endif

<div class="mb-12">
  @includeIf('forms.search', [
      'name' => 'q',
      'value' => $query,
      'action' => get_post_type_archive_link($query_vars['post_type']),
      'placeholder' => pll__('Rechercher un cas producteur', 'rubrash'),
  ])
</div>

<div class="mb-12 grid gap-10 px-7 md:grid-cols-2 lg:grid-cols-3 lg:pl-[10%] lg:pr-[2%]">
  @forelse ($posts as $post)
    <x-card-search :post="$post" />
  @empty
    <p class="mb-0 text-[max(1.25rem,min(.2589vw+1.18932rem,1.5rem))] font-medium text-red">
      {{ pll__('Aucun cas producteurs ne correspond à votre recherche.', 'rubrash') }}</p>
  @endforelse
</div>
