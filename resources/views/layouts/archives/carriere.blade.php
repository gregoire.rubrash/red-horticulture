<x-section class="mt-40 flex flex-col gap-6 lg:flex-row">
  <h1 class="flex-0 text-base uppercase">{{ pll__('Rejoindre l\'équipe', 'rubrash') }}</h1>
  <div class="w-full flex-1">
    <x-h2 class="-mt-2">{{ pll__('Nos postes', 'rubrash') }}</x-h2>
    <ul class="mt-12 flex w-full flex-col">
      @forelse ($jobs as $job)
        <li
          class="mb-6 flex flex-col justify-between gap-6 border-t-2 border-black py-6 md:flex-row md:gap-12 lg:pr-[10%] xl:pr-[20%]">
          <p class="hidden text-xl font-semibold leading-[1.2em] lg:block lg:text-2xl">
            {{ ($loop->index < 9 ? '0' : '') . $loop->index + 1 }}</p>
          <x-h3 class="!mb-0 hidden lg:block lg:!font-semibold">{{ $job->title }}</x-h3>

          {{-- Mobile Start --}}
          <div class="flex gap-6 lg:hidden">
            <p class="text-xl font-semibold leading-[1.2em] lg:text-2xl">
              {{ ($loop->index < 9 ? '0' : '') . $loop->index + 1 }}</p>
            <div>
              <x-h3 class="!mb-0 lg:!font-semibold">{{ $job->title }}</x-h3>
              <p class="mb-2 uppercase text-red">
                <span>{{ $job->city }}</span>
                @if (!empty($job->contracts))
                  <span>- {{ implode(', ', $job->contracts) }}</span>
                @endif
              </p>
            </div>
          </div>
          {{-- Mobile End --}}

          <div class="flex w-full flex-col md:w-auto md:min-w-[294px]">
            <p class="mb-2 hidden uppercase text-red lg:block">
              <span>{{ $job->city }}</span>
              @if (!empty($job->contracts))
                <span>- {{ implode(', ', $job->contracts) }}</span>
              @endif
            </p>
            <x-link :href="$job->link" class="self-end md:w-full"
              slotClass="pr-6 lg:pr-0">{{ pll__('Postuler', 'rubrash') }}</x-link>
          </div>
        </li>
      @empty
        <li>{{ pll__('Aucun poste disponible pour le moment', 'rubrash') }}</li>
      @endforelse
    </ul>
  </div>
</x-section>
