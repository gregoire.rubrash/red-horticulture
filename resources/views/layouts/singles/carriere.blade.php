@php
  $id_form = get_field('career_form_id', 'options');
@endphp

<x-section class="mt-40 flex flex-col gap-12 xl:flex-row">
  <div class="flex-1">
    <a href="{{ get_post_type_archive_link('carriere') }}" class="mb-6 flex items-center gap-2 uppercase text-red">
      <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18" fill="none"
        class="rotate-180 text-red">
        <path d="M11.5 1.5L19 9M19 9L11.5 16.5M19 9L1 9" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
      </svg>
      <span>{{ pll__('Retour', 'rubrash') }}</span>
    </a>
    <x-h1 class="mb-1 font-medium">{{ pll__('Vous postulez ! ', 'rubrash') }}</x-h1>
    <h2 class="mb-3 text-xl font-medium leading-[1.2em] lg:text-2xl">{!! $title !!}</h2>
    <p class="font-semibold text-red">
      <span>{!! $city !!}</span>
      @if (!empty($contracts))
        <span>- {{ implode(', ', $contracts) }}</span>
      @endif
    </p>
    <p class="mb-3 font-semibold text-red">{{ $when }}</p>

    @php(the_content())
  </div>
  <div class="b-form mt-[10%] w-full flex-1">
    <span class="mb-4 block w-full text-right text-gray-500">*{{ pll__('champs obligatoires', 'rubrash') }}</span>
    @if (!empty($id_form))
      {!! gravity_form($id_form, false, false, false) !!}
    @endif
  </div>
</x-section>
