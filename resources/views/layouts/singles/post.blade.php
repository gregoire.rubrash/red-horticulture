<x-content-full-img :title="$title" :content="$excerpt" :image="$thumbnail" :categories="$categories" :date="$date"
  alignDate="left" :isArchive="true" :hasBg="true" />

@php(the_content())
