<x-content-full-img :title="$title" :content="$excerpt" :image="$thumbnail" :date="$date" :hasBg="true" />

@php(the_content())
