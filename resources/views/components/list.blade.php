@props(['items'])

<div class="c-list">
  @foreach ($items as $item)
    <div class="c-list__item flex w-full gap-10 border-t-2 border-black py-6 lg:gap-20">
      @if (!empty($item['icon']))
        <x-picture :url="$item['icon']['url']" alt="{{ $item['icon']['alt'] }}" imageClass="w-16 lg:w-20"></x-picture>
      @else
        <span class="ml-2 font-bold lg:ml-4">0{{ $loop->index + 1 }}</span>
      @endif
      @if (!empty($item['title']) || !empty($item['content']))
        <div class="c-list__container max-w-[90%] lg:max-w-[70%]">
          @if (!empty($item['title']))
            <x-h3>{{ $item['title'] }}</x-h3>
          @endif
          @if (!empty($item['content']))
            <div @class(['list-content', 'mt-4' => !empty($item['title'])])>{!! $item['content'] !!}</div>
          @endif
        </div>
      @endif
      @if (!empty($item['choice_link']))
        @php
          $isFile = (bool) $item['choice_file'] === true ? 'rotate-90' : '';
        @endphp
        <a href="{{ $item['choice_file'] && !empty($item['file']) ? $item['file']['url'] : $item['link']['url'] }}"
          class='relative w-full max-w-[80%] pr-10 text-xl lg:text-2xl'
          {{ $item['choice_file'] && !empty($item['file']) ? 'download' : '' }}>
          <span
            class="max-w-[85%]">{{ $item['choice_file'] && !empty($item['file']) ? $item['file']['title'] : $item['link']['title'] }}</span>
          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18" fill="none"
            @class(['absolute right-0 top-2 $isFile'])>
            <path d="M11.5 1.5L19 9M19 9L11.5 16.5M19 9L1 9" stroke="currentColor" stroke-width="1.5"
              stroke-linecap="round" stroke-linejoin="round" />
          </svg>
        </a>
      @endif
    </div>
  @endforeach
</div>
