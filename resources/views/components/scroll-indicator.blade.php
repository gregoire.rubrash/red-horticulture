<div class="fixed left-4 bottom-[45%] z-40 flex-col justify-center items-center hidden lg:flex">
    <div id="scrollIndicatorLabel"
        class="mb-8 general-sans-bold text-[10px] [writing-mode:vertical-lr] rotate-180 bottom-4">Scroll
    </div>
    <div class="relative overflow-hidden bg-black h-32 w-[3px] rounded-[12px]">
        <div id="progressionBar" class="-translate-y-full bg-red h-full absolute -top-full left-0 right-0 rounded-[12px]">
        </div>
    </div>
</div>
