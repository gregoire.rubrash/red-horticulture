@props(['isFullscreen' => false])

<section {{ $attributes->merge(['class' => ' ' . ($isFullscreen ? '' : 'px-7 lg:pl-[10%] lg:pr-[2%] ')]) }}>
  {!! $slot !!}
</section>
