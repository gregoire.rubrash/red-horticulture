<h3
  {{ $attributes->merge(['class' => 'lg:text-2xl text-xl 2xl:text-3xl leading-[1.2em] font-semibold lg:font-medium']) }}>
  {!! $slot !!}
</h3>
