@props(['url', 'alt'])

@if (!empty($url))
  <picture {{ $attributes->merge(['class' => $pictureClass]) }}>
    <img {{ $attributes->merge(['class' => $imageClass]) }} src="{{ $url }}"
      @if (!empty($alt)) alt="{{ $alt }}" @endif />
  </picture>
@endif
