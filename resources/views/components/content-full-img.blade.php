@props([
    'title',
    'content',
    'image',
    'link',
    'date' => null,
    'alignDate' => 'right',
    'alignRight' => false,
    'categories' => [],
    'isArchive' => false,
    'hasBg' => false,
])

<div @class([
    'relative flex flex-col items-center overflow-hidden lg:min-h-[80dvh] lg:flex-row lg:px-[10%]',
    $alignRight ? 'justify-end' : 'justify-start',
])>
  @if (!empty($image))
    <x-picture :url="$image->src" :alt="$image->alt"
      pictureClass="relative w-full aspect-video h-full lg:absolute lg:top-0 lg:left-0"
      imageClass="absolute inset-0 -z-1 h-full w-full object-cover object-center" />
  @endif
  <div @class([
      'relative p-7 lg:min-h-[400px] lg:w-[605px] lg:p-14',
      'bg-white/75' => $hasBg,
      'text-black lg:text-white' => !$hasBg,
  ])>
    @if (!empty($categories) && $isArchive)
      <div class="bottom-full left-0 flex flex-wrap gap-2 lg:absolute">
        @foreach ($categories as $category)
          <div @class([
              'p-2 text-white',
              $loop->index % 2 == 0 ? 'bg-red' : 'bg-yellow',
          ])>
            {{ $category->name }}</div>
        @endforeach
      </div>
    @endif
    @if (!empty($title))
      <x-h2>{!! $title !!}</x-h2>
    @endif
    @if (!empty($content))
      <p class="my-6">{!! $content !!}</p>
    @endif
    @if (!empty($link))
      <x-link :href="$isArchive ? $link : $link['url']" class="mt-8 block h-fit w-full" slotClass="inline-block md:pr-28">
        @if ($isArchive)
          {{ pll__('Lire l\'article', 'rubrash') }}
        @else
          {{ $link['title'] }}
        @endif
      </x-link>
    @endif
    @if (!empty($date))
      <p @class([
          'mt-8 text-sm capitalize text-black',
          'text-right' => $alignDate == 'right',
          'text-white' => !$hasBg,
      ])>
        {{ $date }}
      </p>
    @endif
  </div>
</div>
