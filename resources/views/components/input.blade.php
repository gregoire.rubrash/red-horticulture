@props(['isWhite' => false, 'required' => false, 'name' => null])
@php
    $placeholder = ucfirst($name . ($required ? '*' : ''));
@endphp
<input type="text" placeholder={{ $placeholder }} {{ $required ? 'required' : '' }}
    {{ $attributes->merge(['class' => ($isWhite ? 'text-white border-white placeholder:text-white' : 'text-black border-black') . ' bg-transparent border-b-[1px] rounded-none pb-2 outline-none grow']) }} />
