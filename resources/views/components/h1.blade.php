<h1 {{ $attributes->merge(['class' => 'lg:text-[3.3rem] text-[2.8rem] font-bold leading-[1.2em] mb-4']) }}>
  {!! $slot !!}
</h1>
