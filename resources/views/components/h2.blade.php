<h2 {{ $attributes->merge(['class' => 'mb-4 text-4xl font-medium leading-[1.2em] lg:text-5xl lg:leading-[1.2em]']) }}>
  {!! $slot !!}
</h2>
