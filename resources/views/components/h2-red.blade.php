@props(['over' => null])
<x-h2 class="text-red pb-4 !mb-6 border-b-[1px] border-red">{!! $slot !!}
    @if ($over)
        <span class="text-sm font-normal">/{{ $over }}</span>
    @endif
</x-h2>
