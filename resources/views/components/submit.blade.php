@props(['isWhite' => false, 'href' => null])
<button type="submit" role="button" title="{{ $slot }}" aria-label="{{ $slot }}"
    @if ($href) href="{{ $href }}" @endif
    {{ $attributes->merge(['class' => ($isWhite ? 'text-white border-white placeholder:text-white' : 'text-red') . ' group relative min-w-[200px] pb-2 pr-10 text-left text-2xl mb-4 w-full lg:w-fit']) }}>
    <div
        {{ $attributes->merge(['class' => ($isWhite ? 'bg-white' : 'bg-red') . ' absolute bottom-0 left-0 h-[1px] w-full']) }}>
    </div>
    {!! $slot !!}
    @if ($isWhite)
        <img src="{{ asset('images/svg/arrow-white.svg') }}" alt='arrow'
            class="absolute right-0 top-2 group-hover:animate-fade" />
    @else
        <img src="{{ asset('images/svg/arrow-red.svg') }}" alt='arrow'
            class="absolute right-0 top-2 group-hover:animate-fade" />
    @endif
</button>
