@props(['href', 'slotClass' => '', 'iconClass' => ''])

@if (!empty($href))
  <a href="{{ $href }}"
    {{ $attributes->merge(['class' => 'relative pb-2 pr-10 text-xl after:content-[""] after:absolute after:bottom-0 after:left-0 after:h-[1px] after:w-full after:bg-current lg:text-2xl cursor-pointer']) }}
    {{ $attributes }}>
    <span @if (!empty($slotClass)) class="{{ $slotClass }}" @endif>{{ $slot }}</span>
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18" fill="none"
      @class(['absolute right-0 top-2', $iconClass => !empty($iconClass)])>
      <path d="M11.5 1.5L19 9M19 9L11.5 16.5M19 9L1 9" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
        stroke-linejoin="round" />
    </svg>
  </a>
@endif
