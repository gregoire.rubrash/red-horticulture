@props(['post', 'showCategories' => false])

<article class="article flex h-full flex-col gap-2">
  <div class="flex w-full flex-1 flex-col gap-2">
    @if (!empty($post->hasThumbnail) && !empty($post->link))
      <a href="{{ $post->link }}" class="relative aspect-video w-full">
        <x-picture :url="$post->thumbnail->src" :alt="$post->thumbnail->alt" pictureClass="w-full h-full"
          imageClass="absolute inset-0 h-full w-full object-cover object-center z-5" />
        @if (!empty($post->categories) && $showCategories)
          <div class="absolute bottom-0 left-0 z-10 flex flex-wrap items-center gap-2">
            @foreach ($post->categories as $category)
              <div @class([
                  'p-2 text-white',
                  $loop->index % 2 == 0 ? 'bg-red' : 'bg-yellow',
              ])>
                {{ $category->name }}</div>
            @endforeach
          </div>
        @endif
      </a>
    @endif
    @if (!empty($post->link) && !empty($post->title))
      <a href="{{ $post->link }}">
        <h3 class="text-18 font-bold">{{ $post->title }}</h3>
      </a>
    @endif
    @if (!empty($post->excerpt))
      <p>{!! $post->excerpt !!}</p>
    @endif
  </div>
  <div class="flex-0 w-full">
    @if (!empty($post->link))
      <x-link :href="$post->link" class="inline-block w-full">{{ pll__('En savoir plus', 'rubrash') }}</x-link>
    @endif
  </div>
</article>
