<h4 {{ $attributes->merge(['class' => 'text-base 2xl:text-2xl uppercase font-bold leading-[1.6em] mb-2 lg:mb-8']) }}>
  {!! $slot !!}</h4>
