<h5 {{ $attributes->merge(['class' => 'lg:text-xl 2xl:text-2xl text-base leading-[1.2em]']) }}>{!! $slot !!}
</h5>
