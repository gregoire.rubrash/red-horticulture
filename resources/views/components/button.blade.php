@props(['href' => null, 'isWhite' => false, 'target' => null])
@if ($href)
  <a href="{{ $href }}" @if ($target === '_blank') target="_blank" @endif>
@endif
<button type="button" role="button" title="{{ $slot }}" aria-label="{{ $slot }}"
  {{ $attributes->merge(['class' => 'relative w-full pb-2 pr-10 text-left lg:text-2xl text-xl mb-4']) }}>
  <div class="{{ $isWhite ? 'bg-white' : 'bg-black' }} absolute bottom-0 left-0 h-[1px] w-full"></div>
  <span class="{{ $isWhite ? 'text-white' : 'text-black' }}">
    {!! $slot !!}
  </span>
  @if ($isWhite)
    <img src="{{ asset('images/svg/arrow-white.svg') }}" alt='arrow' class="absolute right-0 top-2" />
  @else
    <img src="{{ asset('images/svg/arrow.svg') }}" alt='arrow' class="absolute right-0 top-2" />
  @endif
</button>
@if ($href)
  </a>
@endif
