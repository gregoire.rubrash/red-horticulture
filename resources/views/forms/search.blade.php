@if (!empty($placeholder))
  @php
    $value = $value ?? get_search_query();
    $name = $name ?? 's';
    $action = $action ?? home_url('/');
  @endphp

  <div class="flex w-full flex-col bg-red px-7 pb-7 pt-10 text-white md:flex-row lg:pl-[10%] lg:pr-[2%]">
    <p class="flex-0 font-semibold uppercase md:w-60">{!! pll__('Recherche', 'rubrash') !!}</p>
    <form role="search" method="get" class="w-full flex-1" action="{{ $action }}">
      <div class="flex gap-4 border-b border-white pb-2">
        <button type="submit"
          class="aspect-[19/23] w-6 outline-none outline outline-2 outline-offset-2 focus:outline-white">
          <span class="sr-only">{!! pll__('Rechercher', 'rubrash') !!}:</span>
          <svg class="h-full w-full" width="19" height="23" viewBox="0 0 19 23" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <circle cx="9.31486" cy="9.31525" r="6.31895" transform="rotate(-30 9.31486 9.31525)" stroke="white" />
            <path
              d="M16.3278 22.2502C16.4659 22.4893 16.7717 22.5713 17.0108 22.4332C17.25 22.2951 17.3319 21.9893 17.1939 21.7502L16.3278 22.2502ZM12.1256 14.9717L16.3278 22.2502L17.1939 21.7502L12.9916 14.4717L12.1256 14.9717Z"
              fill="white" />
          </svg>
        </button>
        <label class="sr-only">{!! pll__('Rechercher', 'rubrash') !!}:</label>
        <input type="search"
          class="w-full appearance-none bg-transparent placeholder-opacity-50 outline-none outline outline-2 outline-offset-2 placeholder:text-white focus-visible:outline-white"
          placeholder="{!! $placeholder . '&hellip;' !!}" value="{{ $value }}" name="{{ $name }}">
      </div>
    </form>
  </div>
@endif
