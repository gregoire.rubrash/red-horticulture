{{--
  Title: Timeline
  Category: common
  Icon: editor-underline
  Align: full
  Mode: edit  
--}}

@if (!empty($timeline))
  <x-section data-module-timeline id="timeline">
    @if (!empty($title))
      <div class="b-timeline__header flex flex-col lg:flex-row lg:items-center lg:gap-20">
        <x-h2>{!! $title !!}</x-h2>
        @if (!empty($content))
          <div class="b-timeline__content">{!! $content !!}</div>
        @endif
      </div>
    @endif
    <div class="b-timeline__container relative mt-10 overflow-hidden">
      <span class="path absolute left-0 top-[30%] block h-[2px] w-full bg-red"></span>
      <div class="b-timeline__wrapper flex w-max gap-10">
        @foreach ($timeline as $item)
          <div class="b-timeline__item flex max-h-full w-[350px] flex-col items-center gap-6">
            <div class="b-timeline__item--title text-center font-bold">{{ $item['title'] }}</div>
            <span class="circle block h-4 w-4 rounded-full bg-red"></span>
            <div @class([
                'b-timeline__item--content',
                'text-center font-bold' => $choice_center,
            ])>{!! $item['content'] !!}</div>
          </div>
        @endforeach
      </div>
    </div>
  </x-section>
@endif
