{{--
  Title: Nos solutions
  Category: common
  Icon: editor-paragraph
  Align: full
  Mode: edit
--}}

<x-section>
  <div class="lg:max-w-[31rem]">
    @if (!empty($title))
      <x-h2>{{ $title }}</x-h2>
    @endif
    @if (!empty($text))
      <p class="mb-8">{{ $text }}</p>
    @endif
  </div>
  @if (!empty($list))
    <div class="grid grid-cols-2 gap-4 md:grid-cols-4 lg:grid-cols-6">
      @foreach ($list as $item)
        @if (!empty($item) && !empty($item->title) && !empty($item->link))
          <a class="scale-on-hover" href="{{ $item->link }}">
            <div class="flex flex-col justify-center gap-4">
              @if (!empty($item->image))
                <x-picture class="relative aspect-square w-full overflow-hidden" :url="$item->image->url" :alt="$item->image->alt" />
              @endif
              <x-h5 class="text-center">{{ $item->title }}</x-h5>
            </div>
          </a>
        @endif
      @endforeach
    </div>
  @endif
  @if (!empty($link) && !empty($link['url']) && !empty($link['title']))
    <div class="flex w-full items-center justify-end">
      <x-link :href="$link['url']" class="mt-6 w-full md:mt-12 md:w-auto"
        slotClass="inline-block md:pr-28">{{ $link['title'] }}</x-link>
    </div>
  @endif
</x-section>
