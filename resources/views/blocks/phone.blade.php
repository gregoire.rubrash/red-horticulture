{{--
  Title: Téléphone
  Category: common
  Icon: phone
  Align: full
  Mode: edit
--}}

<div @if (!empty($background)) style="--bg: url({{ $background }});" @endif
  class="bg-phone lg:justify-start' flex w-full items-center justify-center">
  <div @class([
      'flex flex-col-reverse w-full justify-center items-center gap-7 px-7 pt-7 lg:flex-row lg:p-0 lg:px-[10%]',
      'lg:justify-between' => $showLinks,
  ])>
    @if (!empty($image))
      <x-picture pictureClass="inline-block aspect-square w-full max-w-[367px] lg:h-full lg:w-auto lg:max-w-[717px]"
        imageClass="w-full h-full object-cover" :url="$image->url" :alt="$image->alt" />
    @endif

    <div class="flex max-w-[367px] flex-col gap-4 text-white">
      @if (!empty($image))
        <x-h2>{{ $title }}</x-h2>
      @endif
      @if ($showLinks === true)
        @if (!empty($linksDL))
          <div class="grid grid-cols-2 gap-2 sm:gap-6">
            @foreach ($linksDL as $linkDL)
              <a href="{{ $linkDL->link->url }}"
                @if (!empty($linkDL->link->target)) target="{{ $linkDL->link->target }}" @endif>
                @if (!empty($linkDL->link->title))
                  <span class="sr-only">{{ $linkDL->link->title }}</span>
                @endif

                @if (!empty($linkDL->image))
                  <x-picture :url="$linkDL->image->url" :alt="$linkDL->image->alt" />
                @endif
              </a>
            @endforeach
          </div>
        @endif
      @else
        @if (!empty($text))
          <p>{{ $text }}</p>
        @endif
      @endif
      @if (!empty($link))
        <x-link :href="$link['url']" @class(['w-full', 'lg:mt-20' => $showLinks])>{{ $link['title'] }}</x-link>
      @endif
    </div>
  </div>
</div>
