{{--
  Title: Liste
  Category: common
  Icon: editor-ul
  Align: full
  Mode: edit
--}}

@if (!empty($title))
  <x-section class="b-list flex flex-col gap-4 lg:flex-row">
    <x-h4 class="mb-4 w-full lg:mb-0 lg:max-w-[35%]">{{ $title }}</x-h4>
    <div class="b-list__container w-full">
      <x-list :items="$list['list']"></x-list>
      @if (!empty($content_bottom))
        <div class="b-list__content-bottom mt-2">{!! $content_bottom !!}</div>
      @endif
    </div>
  </x-section>
@endif
