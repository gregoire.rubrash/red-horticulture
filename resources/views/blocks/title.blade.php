{{--
  Title: Titre
  Category: common
  Icon: editor-paragraph
  Align: full
  Mode: edit
--}}

@if (!empty($content))
  <div class="px-7 lg:px-[10%]">
    @switch($level)
      @case(1)
        <x-h1 @class([
            'text-center' => $position === 'center',
            'text-right' => $position === 'right',
        ])>{{ $content }}</x-h1>
      @break

      @case(2)
        <x-h2 @class([
            'text-center' => $position === 'center',
            'text-right' => $position === 'right',
        ])>{{ $content }}</x-h2>
      @break

      @case(3)
        <x-h3 @class([
            'text-center' => $position === 'center',
            'text-right' => $position === 'right',
        ])>{{ $content }}</x-h3>
      @break

      @case(4)
        <x-h4 @class([
            'text-center' => $position === 'center',
            'text-right' => $position === 'right',
        ])>{{ $content }}</x-h4>
      @break

      @case(5)
        <x-h5 @class([
            'text-center' => $position === 'center',
            'text-right' => $position === 'right',
        ])>{{ $content }}</x-h5>
      @break

      @default
        <p @class([
            'text-center' => $position === 'center',
            'text-right' => $position === 'right',
        ])>{{ $content }}</p>
      @break
    @endswitch
  </div>
@endif
