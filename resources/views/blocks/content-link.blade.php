{{--
  Title: Contenu avec lien
  Category: common
  Icon: text
  Align: full
  Mode: edit
--}}

@if (!empty($title) && !empty($content))
  <x-section class="b-content-link gap-6 lg:flex lg:justify-between">
    <div @class([
        'b-content-link__content mb-4 lg:mb-0 gap-4',
        'w-full flex lg:flex-row flex-col' => empty($link),
        'lg:max-w-[45%]' => !empty($link),
    ])>
      <x-h4 @class(['lg:w-[40%]' => empty($link)])>{!! $title !!}</x-h4>
      <div @class(['b-content-link__content__inner', 'lg:w-full' => empty($link)])>{!! $content !!}</div>
    </div>
    @if (!empty($link))
      <x-link :href="$link['url']" class="mt-8 block h-fit w-full lg:mt-auto lg:w-[40%]"
        slotClass="inline-block md:pr-28">{{ $link['title'] }}</x-link>
    @endif
  </x-section>
@endif
