{{--
  Title: Cards image
  Category: common
  Icon: images-alt
  Align: full
  Mode: edit
--}}

@if (!empty($cards))
  <x-section class="b-list-cards">
    @if (!empty($title))
      <x-h2 class="mb-6 lg:max-w-[50%]">{{ $title }}</x-h2>
    @endif
    <div class="b-list-cards__container w-full">
      <div class="b-list-cards__items flex gap-4 overflow-x-auto lg:grid lg:grid-cols-3">
        @foreach ($cards as $card)
          <div class="b-list-cards__item lg:shrink-initial relative max-h-[67dvh] w-[80%] shrink-0 lg:w-full">
            <x-picture :url="$card['image']['url']" :alt="$card['image']['alt']"
              imageClass="h-[10rem] lg:h-[15rem] w-full object-cover"></x-picture>
            <div class="b-list-cards__content mt-2 w-full lg:mt-4">
              <x-h3>{{ $card['title'] }}</x-h3>
              <p>{{ $card['content'] }}</p>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </x-section>
@endif
