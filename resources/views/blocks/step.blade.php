{{--
  Title: Étape
  Category: common
  Icon: chart-line
  Align: full
  Mode: edit
--}}

<div data-module-step>
  <x-section class="hidden w-full lg:inline-block">
    @foreach ($steps as $step)
      <div @class([
          'mx-auto flex h-full max-w-[1300px] flex-wrap lg:items-center',
          'lg:-my-16' => $choice_lines,
          'lg:mb-24' => !$choice_lines && !$loop->last,
      ])>
        <div class="{{ $loop->index % 2 == 0 ? '' : 'lg:order-2' }} lg:w-1/3">
          <div class="mb-6 border-b-[1px] border-red pb-6 text-3xl font-bold text-red lg:text-4xl">
            0{{ $loop->index + 1 }}<span class="text-sm font-normal">/{{ $loop->count }}</span></div>
          <x-h2>{{ $step['title'] }}</x-h2>
          <div>{!! $step['content'] !!}</div>
        </div>
        <div class="relative lg:w-2/3">
          <x-picture :url="$step['image']['url']" :alt="$step['image']['alt']"
            pictureClass="{{ $loop->index % 2 == 0 ? 'mr-auto' : 'ml-auto' }}"
            imageClass="max-h-[70dvh] object-contain object-center {{ $loop->index % 2 == 0 ? 'ml-auto mr-auto' : 'mr-auto' }} {{ !$choice_lines ? 'max-w-[80%]' : '' }}"></x-picture>
          @if (!$loop->last && $choice_lines)
            @if ($loop->index % 2 !== 0)
              <div class="absolute left-80 top-full -z-1 hidden w-3/4 -translate-y-1/2 -scale-x-100 lg:block">
                <svg width="100%" viewBox="0 0 442 520" fill="none" version="1.1" id="svg1"
                  xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">
                  <defs id="defs1" />
                  <path d="m 428.5,0.5 v 240 c 0,11.598 -9.402,21 -21,21 H 34 c -11.598,0 -21,9.402 -21,21 v 235.79412"
                    stroke="#d41a1a" stroke-width="26" id="path1" />
                </svg>
              </div>
            @elseif (!$loop->last)
              <div class="absolute right-[24rem] top-full -z-1 hidden w-3/4 -translate-y-1/2 lg:block">
                <svg width="100%" viewBox="0 0 442 520" fill="none" version="1.1" id="svg1"
                  xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">
                  <defs id="defs1" />
                  <path d="m 428.5,0.5 v 240 c 0,11.598 -9.402,21 -21,21 H 34 c -11.598,0 -21,9.402 -21,21 v 235.79412"
                    stroke="#d41a1a" stroke-width="26" id="path1" />
                </svg>
              </div>
            @endif
          @endif
        </div>
      </div>
    @endforeach
  </x-section>
  <x-section isFullscreen class="swiper parc-slider lg:hidden" data-step="slider">
    <div class="swiper-wrapper">
      @foreach ($steps as $step)
        <div class="swiper-slide px-7">
          <div @class([
              'mx-auto flex max-w-[1300px] flex-wrap items-center',
              'lg:-my-16' => $choice_lines,
              'lg:mb-10' => !$choice_lines,
          ])>
            <div class="{{ $loop->index % 2 == 0 ? 'lg:order-2' : '' }} lg:w-1/3">
              <div class="mb-6 border-b-[1px] border-red pb-6 text-3xl font-bold text-red lg:text-4xl">
                {{ $loop->index + 1 }}<span class="text-sm font-normal">/{{ $loop->count }}</span></div>
              <x-h2>{{ $step['title'] }}</x-h2>
              <div>{!! $step['content'] !!}</div>
            </div>
            <div class="relative lg:w-2/3">
              <x-picture :url="$step['image']['url']" :alt="$step['image']['alt']"
                pictureClass="{{ $loop->index % 2 == 0 ? 'mr-auto' : 'ml-auto' }}"></x-picture>
              @if (!$loop->last && $choice_lines)
                @if ($loop->index % 2 == 0)
                  <div class="absolute left-80 top-full -z-1 hidden w-3/4 -translate-y-1/2 -scale-x-100 lg:block">
                    <svg width="100%" viewBox="0 0 442 520" fill="none" version="1.1" id="svg1"
                      xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">
                      <defs id="defs1" />
                      <path
                        d="m 428.5,0.5 v 240 c 0,11.598 -9.402,21 -21,21 H 34 c -11.598,0 -21,9.402 -21,21 v 235.79412"
                        stroke="#d41a1a" stroke-width="26" id="path1" />
                    </svg>
                  </div>
                @elseif ($loop->last)
                  <div class="absolute right-72 top-full -z-1 hidden w-3/4 -translate-y-1/2 lg:block">
                    <svg width="100%" viewBox="0 0 442 520" fill="none" version="1.1" id="svg1"
                      xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">
                      <defs id="defs1" />
                      <path
                        d="m 428.5,0.5 v 240 c 0,11.598 -9.402,21 -21,21 H 34 c -11.598,0 -21,9.402 -21,21 v 235.79412"
                        stroke="#d41a1a" stroke-width="26" id="path1" />
                    </svg>
                  </div>
                @endif
              @endif
            </div>
          </div>
        </div>
      @endforeach
    </div>
    <div class="ml-auto flex justify-end gap-2 px-7 pt-4">
      <div class="parc-swiper-button-prev"><img class="w-16 rotate-180" src={{ asset('images/svg/next.svg') }}
          alt="bouton précédent" /></div>
      <div class="parc-swiper-button-next"><img class="w-16" src={{ asset('images/svg/next.svg') }}
          alt="bouton suivant" /></div>
    </div>
    <div class="mb-12"></div>
  </x-section>
</div>
