{{--
  Title: Contenu mis en avant
  Category: common
  Icon: editor-paragraph
  Align: full
  Mode: edit
--}}

@if (!empty($repeaterContent))
  <section class="b-highlight-content">
    @if (!empty($image))
      <x-picture :url="$image['url']" :alt="$image['alt']"
        class="h:[40dvh] w-full object-cover object-center lg:h-[70dvh]"></x-picture>
    @endif
    @if (!empty($repeaterContent))
      <x-section class="b-highlight-content__wrapper gap-6 lg:flex">
        <x-h4 class="mb-4 w-full lg:mb-0 lg:max-w-[15%] lg:!text-base">{{ $title }}</x-h4>
        <div class="b-highlight-content__container w-full">
          @if (!empty($content))
            <div class="b-highlight-content_description mb-4 lg:mb-10">{!! $content !!}</div>
          @endif
          <div class="b-highlight-content__items ml-auto flex w-full flex-col gap-12 lg:flex-row lg:gap-6">
            @foreach ($repeaterContent as $content)
              <div class="b-highlight-content__item w-full text-red lg:text-center">
                <p class="text-[1.75rem] font-bold lg:text-[2.375rem]">{{ $content['title'] }}</p>
                <p class="mt-1">{!! $content['content'] !!}</p>
              </div>
            @endforeach
          </div>
        </div>
      </x-section>
    @endif
  </section>
@endif
