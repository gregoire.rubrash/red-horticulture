{{--
  Title: Texte
  Category: common
  Icon: editor-paragraph
  Align: full
  Mode: edit
--}}

@if (!empty($text))
  <div class="t-wysiwyg wrapper">
    {!! $text !!}
  </div>
@endif
