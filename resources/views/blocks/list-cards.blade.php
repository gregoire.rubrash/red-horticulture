{{--
  Title: Liste de cards
  Category: common
  Icon: images-alt
  Align: full
  Mode: edit
--}}

@if (!empty($cards))
  <section class="b-list-cards mx-4 lg:mx-20">
    @if (!empty($title))
      <x-h2 class="mb-6 lg:max-w-[50%]">{{ $title }}</x-h2>
    @endif
    <div class="b-list-cards__container w-full">
      <div class="b-list-cards__items flex gap-4 overflow-x-auto lg:grid lg:grid-cols-4">
        @foreach ($cards as $card)
          <div class="b-list-cards__item lg:shrink-initial relative max-h-[67dvh] w-[80%] shrink-0 lg:w-full">
            <x-picture :url="$card['image']['url']" :alt="$card['image']['alt']" imageClass="h-[67dvh] w-full object-cover"></x-picture>
            <div
              class="b-list-cards__content absolute bottom-0 left-0 flex w-full flex-col gap-4 bg-black p-6 text-white">
              <x-h3>{{ $card['title'] }}</x-h3>
              <x-link :href="$card['link']['url']" :iconClass="'path-white'">{{ $card['link']['title'] }}</x-link>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
@endif
