{{--
  Title: Témoignage/avis
  Category: common
  Icon: testimonial
  Align: full
  Mode: edit
--}}

@if (!empty($content))
  <x-section class="b-point-of-view flex flex-col lg:flex-row">
    @if (!empty($suptitle))
      <x-h4 class="mt-5">{{ $suptitle }}</x-h4>
    @endif
    <div class="b-point-of-view__container">
      <div
        class="b-point-of-view__content text-[2.175rem] font-medium leading-[2.825rem] lg:ml-auto lg:max-w-[80%] lg:text-[3rem] lg:leading-[3.625rem]">
        {{ $content }}
      </div>
      @if (!empty($author))
        <div class="b-point-of-view__author ml-auto mt-10 w-fit lg:mr-40 lg:mt-20">
          <p class="text-[1.5rem] font-medium">{{ $author }}</p>
          <p>{{ $position }}</p>
        </div>
      @endif
    </div>
  </x-section>
@endif
