{{--
  Title: Scroll image
  Category: common
  Icon: arrow-down-alt
  Align: full
  Mode: edit
--}}

<x-section isFullscreen @class([
    'flex relative flex-col lg:flex-row first:mt-28',
    'bg-black text-white' => $dark,
])>
  <div @class([
      'relative h-full w-full order-2 overflow-hidden',
      'w-[50%] ml-auto' => $choice_side,
      'lg:w-3/5 lg:order-1' => !$choice_side,
  ])>
    @foreach ($images as $index => $image)
      @foreach ($image as $index => $img)
        <x-picture :url="$img['url']" :alt="$img['alt']" class="w-full"></x-picture>
      @endforeach
    @endforeach
  </div>
  <div @class([
      'w-full pl-6 pr-4 pt-5 lg:pl-[5%] lg:pt-0 order-1 mb-4 lg:mb-0',
      'w-[50%]' => $choice_side,
      'lg:w-[50%] lg:order-2' => !$choice_side,
  ])>
    <div class="sticky left-0 top-32 w-full">
      <x-h2>{!! $title !!}</x-h2>
      {!! $content !!}
      @if (!empty($repeaterContent))
        @foreach ($repeaterContent as $index => $additionalContent)
          <x-h2>{!! $additionalContent['title'] !!}</x-h2>
          {!! $additionalContent['content'] !!}
        @endforeach
      @endif
      @if (!empty($list))
        <div class="mt-4">
          <x-list :items="$list"></x-list>
        </div>
      @endif
    </div>
  </div>
</x-section>
