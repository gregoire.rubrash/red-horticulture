{{--
  Title: Cover
  Category: common
  Icon: cover-image
  Align: full
  Mode: edit
--}}

@if (!empty($image) || !empty($video))
  <section class='b-cover'>
    @if ($choice === true)
      <video class="aspect-video h-full w-full object-cover object-center" src="{{ $video['url'] }}" muted playsinline
        loop></video>
    @else
      <div class="b-cover__image relative h-[40dvh] w-full lg:h-[80dvh]">
        <x-picture :url="$image['url']" :alt="$image['alt']" class="block h-full w-full object-cover object-center">
        </x-picture>
        <x-h1 class="absolute bottom-4 left-0 z-10 h-fit w-full text-center text-white">{{ $title_img }}</x-h1>
      </div>
    @endif
    <div
      class="b-cover__container mt-4 flex flex-wrap px-4 lg:ml-auto lg:mt-[3.75rem] lg:w-[90%] lg:flex-nowrap lg:gap-4 lg:px-0">
      @if (!empty($title))
        <x-h1 class="b-cover__title font-medium">{!! $title !!}</x-h1>
      @endif
      @if (!empty($content))
        <div class="b-cover__content lg:max-w-[64%] lg:pr-4 lg:pt-4">{!! $content !!}</div>
      @endif
    </div>
  </section>
@endif
