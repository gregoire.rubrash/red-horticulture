{{--
  Title: Implantations
  Category: common
  Icon: location
  Align: full
  Mode: edit
--}}

@if (!empty($implantations))
  <section class="b-implantations ml-auto flex flex-col gap-6 px-4 lg:w-[90%] lg:flex-row lg:px-0">
    <div class="b-implantations__content flex-1">
      @if (!empty($suptitle))
        <x-h4>{{ $suptitle }}</x-h4>
      @endif
      @if (!empty($title))
        <x-h2>{{ $title }}</x-h2>
      @endif
      @foreach ($implantations as $implantation)
        <div class="b-implantations__item mb-4 flex gap-6 border-t-2 border-black pt-4">
          <div class="index mt-1 font-bold">0{{ $loop->iteration }}</div>
          <div class="container">
            <x-h3>{{ $implantation['title'] }}</x-h3>
            <div class="address mt-2 flex flex-col justify-between lg:flex-row">
              <p class="lg:max-w-[35%]">{{ $implantation['address'] }}</p>
              <p class="mt-4 lg:mt-0 lg:max-w-[50%]">{{ $implantation['content'] }}</p>
            </div>
          </div>
        </div>
      @endforeach
    </div>
    <div class="b-implantations__image flex-1">
      @if (!empty($image))
        <x-picture :url="$image['url']" :alt="$image['alt']" class="w-full object-cover"></x-picture>
      @endif
    </div>
  </section>
@endif
