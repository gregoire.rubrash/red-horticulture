{{--
  Title: Section présentation produits
  Category: common
  Icon: images-alt
  Align: full
  Mode: edit
--}}

<div data-module-image-tabs>
  <x-section isFullscreen class='relative flex flex-wrap'>
    <div class='relative h-full w-full lg:-order-1 lg:w-3/5'>
      @if (!empty($products))
        @foreach ($products as $index => $product)
          <div class="image-wrap !hidden max-h-[50rem] w-full overflow-hidden lg:block" id="image-{{ $index }}"
            data-image-tabs="image">
            <x-picture :url="$product['image']['url']" :alt="$product['image']['alt']" class="w-full object-cover object-center" />
          </div>
        @endforeach
      @endif
    </div>
    <div class="w-full pl-6 pr-4 pt-5 lg:w-2/5 lg:pl-[10%] lg:pt-0">
      @foreach ($products as $index => $product)
        <div class="mb-6">
          <div data-image-tabs="button" id="product-button-{{ $index }}">
            <x-button data-product-id="{{ $index }}">
              {!! $product['name'] !!}
            </x-button>
          </div>
          <div class="grid transition-all" data-image-tabs="description" class="product-description"
            id="product-description-{{ $index }}">
            <div class="overflow-hidden">
              {!! $product['description'] !!}
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </x-section>
</div>
