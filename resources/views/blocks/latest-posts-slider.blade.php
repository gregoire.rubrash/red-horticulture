{{--
  Title: Slider derniers articles
  Category: common
  Icon: slides
  Align: full
  Mode: edit
--}}

<div data-module-latest-posts-slider>
  <div class="swiper cas-producteurs-slider relative" data-latest-posts-slider="slider">
    <div class="swiper-wrapper relative !h-auto items-center">
      @foreach ($posts as $index => $post)
        @php
          setup_postdata($GLOBALS['post'] = $post);
          $thumbnail_id = get_post_thumbnail_id($post->ID);
          $image_srcset = wp_get_attachment_image_srcset($thumbnail_id);
          $image_sizes = wp_get_attachment_image_sizes($thumbnail_id, 'full');
          $image_src = wp_get_attachment_image_url($thumbnail_id, 'full');
          $post_type_name = get_post_type_object(get_post_type($post))->labels->name;
        @endphp
        <div isFullscreen class='swiper-slide image-static-fade is-right relative !flex flex-wrap'>
          <div class="w-full pl-6 pr-4 pt-5 lg:w-2/5 lg:pl-[10%]">
            <x-h4>{{ $post_type_name }}</x-h4>
            <div class="lg:mb-32"></div>
            <x-h2>{!! the_title() !!}</x-h2>
            <div class="mb-3 lg:mb-0"></div>
            <x-button href="{{ get_permalink($post) }}" class="lg:!my-20">En savoir plus</x-button>
          </div>
          <div class='image-wrap relative -order-1 aspect-square max-h-[40rem] w-full overflow-hidden lg:w-3/5'>
            <img src="{{ esc_url($image_src) }}" alt="{!! the_title() !!}" srcset="{{ esc_attr($image_srcset) }}"
              sizes="{{ esc_attr($image_sizes) }}" class="top-0 aspect-square w-full object-cover lg:absolute" />
          </div>
        </div>
      @endforeach
    </div>
    <div
      class="cas-producteurs-pagination z-50 flex justify-center px-4 lg:absolute lg:!bottom-0 lg:!left-auto lg:right-0 lg:!m-0 lg:!w-[31vw]">
    </div>
  </div>
</div>
