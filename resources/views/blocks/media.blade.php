{{--
  Title: Media
  Category: common
  Icon: editor-paragraph
  Align: full
  Mode: edit
--}}

@if (!empty($image) || !empty($video))
  <section @class(['b-media', 'wrapper' => !empty($choice_size)])>
    @if (!empty($image))
      <x-picture :url="$image['url']" :alt="$image['alt']"
        class="h-[40dvh] w-full object-contain object-center lg:h-full"></x-picture>
    @elseif (!empty($video))
      <video src="{{ $video['url'] }}" class="h-[40dvh] w-full lg:h-screen" controls playsinline muted></video>
    @endif
  </section>
@endif
