{{--
  Title: Contenu avec image plein écran
  Category: common
  Icon: cover-image
  Align: full
  Mode: edit
--}}

<x-content-full-img :title="$title" :content="$content" :image="$image" :alignRight="$alignRight" :link="$link"
  :hasBg="$hasBg" />
