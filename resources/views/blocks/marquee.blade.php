{{--
  Title: Marquee
  Category: common
  Icon: slides
  Align: full
  Mode: edit
--}}

@if (!empty($marquees))
  <div
    style="--_size: clamp(10rem, 1rem + 40vmin, 30rem);--marquee-gap: calc(var(--_size) / 14);--scroll-end: calc(-100% - var(--marquee-gap));">
    <div class="mb-12 px-7 lg:pl-[10%] lg:pr-[2%]">
      <div @class([
          'flex flex-col gap-4 md:flex-row' => !empty($text),
      ])>
        @if (!empty($title))
          <x-h2 @class([
              'max-w-[620px]' => empty($text),
              'max-w-[192px] flex-1 !text-base !font-semibold !mb-0 uppercase' => !empty(
                  $text
              ),
          ])>{{ $title }}</x-h2>
        @endif
        @if (!empty($text))
          <p class="!mb-0 flex-1">{{ $text }}</p>
        @endif
      </div>
    </div>

    <div class="flex flex-col gap-[var(--marquee-gap)] overflow-hidden">
      @foreach ($marquees as $marquee)
        <div class="flex select-none gap-[var(--marquee-gap)] overflow-hidden">
          @for ($i = 0; $i < 2; $i++)
            <div @class([
                'flex gap-[var(--marquee-gap)] min-w-full shrink-0 items-center justify-around',
                'animate-marquee' => $loop->first,
                'animate-marquee-reverse' => $loop->last,
            ])>
              @foreach ($marquee as $image)
                <x-picture imageClass="max-h-[60px] lg:max-h-[90px]" :url="$image->url" :alt="$image->alt" />
              @endforeach
            </div>
          @endfor
        </div>
      @endforeach
    </div>
  </div>
@endif
