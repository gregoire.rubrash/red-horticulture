{{--
  Title: Formulaires
  Category: common
  Icon: editor-paste-text
  Align: full
  Mode: edit
--}}

@if (!empty($id_form))
  <section class="b-form mx-7 mt-28 flex flex-col lg:ml-[10%] lg:mr-0 lg:mt-40 lg:flex-row lg:gap-20">
    <div class="b-form__left lg:w-[50%]">
      <x-h1>{{ $title }}</x-h1>
      @if (!empty($content_form))
        <div class="b-form__content">{!! $content_form !!}</div>
      @endif
      <span class="block w-full text-right text-gray-500">*{{ pll__('champs obligatoires', 'rubrash') }}</span>
      <div class="b-form__form mt-6">
        {!! gravity_form($id_form, false, false, false) !!}
      </div>
    </div>
    @if (!empty($image))
      <x-picture :url="$image->src" :alt="$image->alt" pictureClass="h-[40dvh] mt-20 lg:mt-0 lg:h-[80dvh] lg:w-[50%]"
        imageClass="h-full w-full object-cover"></x-picture>
    @endif
  </section>
@endif
