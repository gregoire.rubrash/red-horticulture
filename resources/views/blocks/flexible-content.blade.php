{{--
  Title: Contenu flexible
  Category: common
  Icon: controls-repeat
  Align: full
  Mode: edit
--}}

@php
  $side = $choice_side ? 'after:bg-gradient-to-r' : 'after:bg-gradient-to-l';
  $gradient = !empty($double_image) ? "after:content-[''] after:absolute after:top-0 after:right-0 after:w-full after:h-full $side after:from-white after:to-transparent after:to-70%" : '';
@endphp

@if (!empty($image) || !empty($title))
  <section @class([
      'b-flexible-content flex lg:flex-row flex-col',
      'items-center h-full' => $choice_text,
      'gap-6 lg:w-[90%] lg:ml-auto' => $choice_side,
      'gap-6 lg:gap-[7.5rem]' => !$choice_side,
      'overflow-hidden' => !empty($double_image),
  ])>
    @if (!empty($image))
      <div @class([
          'b-flexible-content__left h-full relative',
          'order-2 lg:w-[65%]' => $choice_side,
          'lg:w-[60%] order-2 lg:order-1' => !$choice_side,
      ])>
        <x-picture :url="$image['url']" :alt="$image['alt']"
          imageClass="object-cover object-center w-full {{ $choice_height ? 'min-h-[40dvh] max-h-[50dvh] lg:max-h-[60dvh]' : 'min-h-[40dvh] lg:max-h-[80dvh] lg:min-h-[70dvh]' }}"
          pictureClass="{{ $gradient }}"></x-picture>
        @if (!empty($double_image))
          <x-picture :url="$double_image['url']" :alt="$double_image['alt']" pictureClass="absolute top-0 w-full h-full right-0 lg:right-0"
            imageClass="object-cover object-center h-full {{ $choice_side ? 'mr-auto' : 'ml-auto' }}"></x-picture>
        @endif
      </div>
    @endif
    @if (!empty($title))
      <div @class([
          'b-flexible-content__right px-4 lg:px-0',
          'lg:order-1 lg:w-[35%]' => $choice_side,
          'order-1 lg:order-2 lg:mr-6 lg:w-[40%]' => !$choice_side,
      ])>
        <x-h2 @class([
            '!text-[1.5rem] border-b-2 border-black w-fit pb-2' => $choice_title,
        ])>{{ $title }}</x-h2>
        @if (!empty($content))
          <div>{!! $content !!}</div>
        @endif
        @if (!empty($list_links))
          <div class="mt-6 flex flex-col gap-6">
            @foreach ($list_links as $item)
              <div class="flex gap-1 border-t-2 border-black pt-6 text-2xl font-medium">
                <p>{!! $item['content'] !!}</p>
                <a href="{{ $item['link']['url'] }}" class="text-red hover:underline"
                  {{ !empty($item['link']['target']) ? 'target=_blank' : null }}>{!! $item['link']['title'] !!}</a>
              </div>
            @endforeach
          </div>
        @endif
        @if (!empty($link))
          <x-link :href="$link['url']" class="mt-4 block">{!! $link['title'] !!}</x-link>
        @endif
      </div>
    @endif
  </section>
@endif
