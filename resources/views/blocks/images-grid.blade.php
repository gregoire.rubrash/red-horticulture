{{--
  Title: Grille images
  Category: common
  Icon: grid-view
  Align: full
  Mode: edit
--}}

@if (!empty($images))
  <section class="b-images-grid wrapper">
    @if (!empty($title))
      <x-h2>{{ $title }}</x-h2>
    @endif
    @if (!empty($images))
      <div class="b-images-grid__images grid grid-cols-2 gap-3 lg:grid-cols-3 lg:gap-6">
        @foreach ($images as $image)
          @if (!empty($image['link']['url']))
            <a href="{{ $image['link']['url'] }}" class="b-images-grid__image lg:relative">
              <x-picture :url="$image['image']['url']" :alt="$image['image']['alt']" imageClass="object-cover lg:h-full"></x-picture>
              <div
                class="b-images-grid__content mt-2 text-center lg:absolute lg:bottom-0 lg:right-0 lg:mt-0 lg:bg-white lg:p-4 lg:text-left">
                <h3 class="mb-2 text-[1rem] text-red lg:mb-0 lg:text-[1.375rem]">{{ $image['name'] }}</h3>
                <p>{!! $image['label'] !!}</p>
              </div>
            </a>
          @else
            <div class="b-images-grid__image lg:relative">
              <x-picture :url="$image['image']['url']" :alt="$image['image']['alt']" imageClass="object-cover lg:h-full"></x-picture>
              <div
                class="b-images-grid__content mt-2 text-center lg:absolute lg:bottom-0 lg:right-0 lg:mt-0 lg:bg-white lg:p-4 lg:text-left">
                <h3 class="mb-2 text-[1rem] text-red lg:mb-0 lg:text-[1.375rem]">{{ $image['name'] }}</h3>
                <p>{!! $image['label'] !!}</p>
              </div>
            </div>
          @endif
        @endforeach
      </div>
    @endif
  </section>
@endif
