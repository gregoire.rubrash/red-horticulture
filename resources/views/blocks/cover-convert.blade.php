{{--
  Title: Cover convertisseur
  Category: common
  Icon: cover-image
  Align: full
  Mode: edit
--}}

@if (!empty($title) || !empty($image) || !empty($id_form))
  <section class="mt-18 flex flex-col lg:flex-row lg:items-center">
    <x-picture :url="$image->src" :alt="$image->alt"
      pictureClass="lg:w-[55dvw] h-[40dvh] mt-10 lg:mt-0 lg:h-screen order-2 lg:order-1"
      imageClass="h-full w-full object-cover object-center"></x-picture>
    <div class="b-cover-covert__container order-1 mx-6 mt-24 lg:order-2 lg:mr-6 lg:mt-0 lg:w-[40dvw] lg:pl-20">
      <a href="{{ $link['url'] }}" class="mb-6 flex items-center gap-2 uppercase text-red">
        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18" fill="none"
          class="rotate-180 text-red">
          <path d="M11.5 1.5L19 9M19 9L11.5 16.5M19 9L1 9" stroke="currentColor" stroke-width="1.5"
            stroke-linecap="round" stroke-linejoin="round" />
        </svg>
        <span>{{ pll__('Retour', 'rubrash') }}</span>
      </a>
      <x-h1>{{ $title }}</x-h1>
      @if (!empty($content))
        <div class="b-cover-convert__content link-underline">{!! $content !!}</div>
      @endif
      <div class="b-form convert-form mt-8">
        {!! gravity_form($id_form, false, false, false) !!}
      </div>
    </div>
  </section>
@endif
