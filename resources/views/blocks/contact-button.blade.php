{{--
  Title: Button - Contact
  Category: common
  Icon: button
  Align: full
  Mode: edit
--}}

@if (!empty($link))
  <x-section>
    <div @class([
        'flex w-full items-center',
        'justify-center' => $position === 'center',
        'justify-end' => $position === 'right',
    ])>
      <x-link :href="$link['url']" class="text-red sm:!pr-16 sm:!text-4xl sm:after:!h-[3px] lg:!pr-24 lg:text-5xl"
        slotClass="inline-block font-medium sm:h-9 lg:h-12"
        iconClass="aspect-square sm:h-9 sm:w-9 sm:!top-1">{{ $link['title'] }}</x-link>
    </div>
  </x-section>
@endif
