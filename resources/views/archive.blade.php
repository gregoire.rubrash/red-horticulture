@php
  global $wp_query;
  $query_vars = $wp_query->query_vars;
@endphp

@extends('layouts.app')

@section('content')
  @includeIf('layouts.archives.' . $query_vars['post_type'])
@endsection
