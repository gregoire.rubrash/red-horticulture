@php
  $id_form = get_field('footer_form_id', 'options');
@endphp

<footer class="bg-red pb-4 pt-8 text-white lg:pt-24">
  <x-section class="flex flex-wrap gap-8">
    <div class="grow lg:grow-0">
      <x-h2>Contact</x-h2>
      <ul class="mb-4">
        <li>
          <a href="mailto:contact@horticulture.red" target="_blank">contact@horticulture.red</a>
        </li>
        <li>
          <a href="mailto:support@horticulture.red" target="_blank">support@horticulture.red</a>
        </li>
        <li><a href="tel:+33 4 87 91 24 00" target="_blank">+33 4 87 91 24 00</a></li>
        <li>60 Quai Perrache <br />69002 Lyon - France</li>
      </ul>
      <div class="flex items-center gap-4">
        <a href="https://www.instagram.com/REDHorticulture/" target="_blank">
          <img width="30px" height="30px" src={{ asset('images/svg/instagram.svg') }} alt="Instagram" />
        </a>
        <a href="https://fr.linkedin.com/company/redhorticulture" target="_blank">
          <img width="29px" height="28px" src={{ asset('images/svg/linkedin.svg') }} alt="LinkedIn" />
        </a>
        <a href="https://www.youtube.com/channel/UC6MLK3-rUmZdC9NYrQUJ84Q" target="_blank">
          <img width="30px" height="22px" src={{ asset('images/svg/youtube.svg') }} alt="YouTube" />
        </a>
      </div>
    </div>

    <div class="lg:pl-20">
      <x-h2>Liens</x-h2>
      <div class="mt-2">
        <?php wp_nav_menu([
            'theme_location' => 'secondary',
            'menu_class' => 'footer-menu-class block',
        ]); ?>
      </div>
    </div>

    <div class="max-w-[600px] grow lg:ml-auto">
      <x-h2>Notre newsletter</x-h2>
      <div class="b-form news">
        @if (!empty($id_form))
          {!! gravity_form($id_form, false, false, false) !!}
        @endif
      </div>
    </div>
  </x-section>
  <div class="mt-6 px-8 lg:mb-2 lg:mt-0 lg:px-[10%]">Rouge Engineered Designs S.A.S. ©2021 - Tous droits réservés</div>
</footer>
