<header class="header fixed top-0 z-50 mx-auto flex w-full items-center justify-between bg-white p-4" data-module-header>
  <a href="{{ get_home_url() }}" data-header="logo" class="lg:!opacity-100">
    <img src="{{ asset('images/menu/logo.png') }}" alt="RED Horticulture" class="h-[30px] w-[87px]" />
  </a>
  <nav class="hidden gap-10 lg:flex">
    <?php wp_nav_menu(); ?>
    <div id="black-menu-background"
      class="black-menu-background pointer-events-none absolute left-0 top-full -z-5 h-0 w-screen cursor-pointer bg-black/50">
    </div>
  </nav>
  <div class="flex items-center gap-8">
    @includeIf('partials.lang-switcher')
    <a href="/contact" class="hidden lg:block">
      <button type="button" role="button"
        class='rounded-full border-2 border-red px-3 py-1 font-bold text-red'>{{ pll__('Contactez-nous', 'rubrash') }}</button>
    </a>
    <button class="header__burger" data-header="burger">
      <span>{{ pll__('Afficher le menu', 'rubrash') }}</span>
    </button>
  </div>
  <div id="mobile-nav"
    class="absolute left-0 top-full h-screen w-full -translate-x-full overflow-y-scroll bg-white px-10 py-7 lg:hidden">
    <nav class="nav--mobile mb-16">
      <?php wp_nav_menu(); ?>
      <a href="/contact">
        <button type="button" role="button"
          class='-mx-5 rounded-full border-2 border-red px-5 py-4 text-[30px] font-bold text-red'>{{ pll__('Contactez-nous', 'rubrash') }}</button>
      </a>
    </nav>
  </div>
  <a href="{{ get_home_url() }}" id="mobile-nav-go-back"
    class="absolute left-4 top-0 flex h-full w-1/2 items-center opacity-0 lg:hidden">
    <img src="{{ asset('images/svg/arrow.svg') }}" alt="arrow" class="-scale-x-100" />
  </a>
</header>
