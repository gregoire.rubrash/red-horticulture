@extends('layouts.app')

@section('content')
  @while (have_posts())
    @php(the_post())
    @includeIf('layouts.singles.' . get_post_type())
  @endwhile
@endsection
