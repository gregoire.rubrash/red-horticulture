@extends('layouts.app')

@section('content')
  <div class="flex h-full min-h-[80dvh] w-full flex-col items-center justify-center gap-4">
    <h1 class="mb-0 text-[max(5.125rem,min(13.3333vw+2rem,18rem))] leading-[.9] text-red">404</h1>
    <p class="mb-0 text-[max(1.25rem,min(.2589vw+1.18932rem,1.5rem))] font-medium">
      {{ pll__('Page introuvable', 'rubrash') }}</p>
    <p class="mb-0 text-[max(1.25rem,min(.2589vw+1.18932rem,1.5rem))] font-medium">
      {{ pll__(
          'Mais rassurez vous tout espoir n\'est pas
            perdu',
          'rubrash',
      ) }}</p>
  </div>
@endsection
