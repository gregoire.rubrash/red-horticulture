import domReady from '@roots/sage/client/dom-ready';

import { ENV } from './core/config';
import Core from './core/core';
import { supportAspectRatio } from './utils/support';
import { calculateScrollPercentage } from './utils/scroll-indicator';

/**
 * Application entrypoint
 */
domReady(async () => {
  const isSupportAspectRatio = supportAspectRatio();

  if (!isSupportAspectRatio) {
    document.documentElement.classList.add('browser-no-support');
    alert(
      'Your browser does not support aspect-ratio. Unfortunately that means you will not get the full experience from our website and miss out on cool features. We recommend you to update your browser to a modern version!',
    );
  }

  if (ENV.IS_MOBILE) {
    document.documentElement.classList.add('is-mobile');
  }

  const core = new Core();
  core.init();

  /**
   * @author Luca Ciampi
   * moves the scroll bar progression according to % page scrolled
   */

  const progressionBar = document.getElementById('progressionBar');
  window.addEventListener('scroll', function () {
    const scrollPercentage = calculateScrollPercentage();
    progressionBar.style.transform = 'translateY(' + scrollPercentage + '%)';
  });
});

/**
 * @see {@link https://webpack.js.org/api/hot-module-replacement/}
 */
if (import.meta.webpackHot) import.meta.webpackHot.accept(console.error);
