import { CountUp } from 'countup.js';

const observerOptions = {
  root: null,
  rootMargin: '0px',
  threshold: 0.3,
};

const countersObserverOptions = {
  root: null,
  rootMargin: '0px',
  threshold: 0.8,
};

observeImageSections(observerOptions);
observeVideos(observerOptions);
observeCounters(countersObserverOptions);

/**
 * Starts counter for red-key-point component
 */
function activateCounter(redkeypoint) {
  const targetValue = parseInt(redkeypoint.getAttribute('data-target'));

  if (isNaN(targetValue)) return;

  const countUp = new CountUp(redkeypoint, targetValue);

  if (!countUp.error) {
    countUp.start();
  } else {
    console.error(countUp.error);
  }
}

/**
 * Triggers image side fade animation on scroll for "image-section" component
 */
function observeImageSections(observerOptions) {
  const observerCallback = (entries) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        entry.target.classList.add('fade--revealed');
      } else {
        entry.target.classList.remove('fade--revealed');
      }
    });
  };

  const observer = new IntersectionObserver(observerCallback, observerOptions);
  const fadeElements = document.querySelectorAll('.image-fade');

  if (
    typeof fadeElements === 'object' &&
    !Array.isArray(fadeElements) &&
    fadeElements !== null
  ) {
    fadeElements.forEach((ref) => {
      if (ref) {
        if (ref.classList.contains('image-fade--right')) {
          ref.classList.add('fade-right');
        } else {
          ref.classList.add('fade-left');
        }
        observer.observe(ref);
      }
    });
  }
}

/**
 * Triggers video autoplay on user scroll
 */
function observeVideos(observerOptions) {
  const videos = document.querySelectorAll('video');
  const videosObserverCallback = (entries) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        entry.target.play();
      } else {
        entry.target.pause();
      }
    });
  };

  const videosObserver = new IntersectionObserver(
    videosObserverCallback,
    observerOptions,
  );

  if (videos !== null) {
    videos.forEach((ref) => {
      if (ref) {
        videosObserver.observe(ref);
      }
    });
  }
}

/**
 * Triggers counters on user scroll
 */
function observeCounters(observerOptions) {
  const redKeyPoints = document.querySelectorAll('.counter');
  const countersObserverCallback = (entries) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        activateCounter(entry.target);
      }
    });
  };

  const countersObserver = new IntersectionObserver(
    countersObserverCallback,
    observerOptions,
  );

  if (redKeyPoints !== null) {
    redKeyPoints.forEach((ref) => {
      if (ref) {
        countersObserver.observe(ref);
      }
    });
  }
}

/**
 * Retrieves the percentage of length page scrolled
 * @returns a number between 0 and 1
 */
export function calculateScrollPercentage() {
  const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
  const scrollHeight =
    document.documentElement.scrollHeight -
    document.documentElement.clientHeight;
  const scrollPercentage = (scrollTop / scrollHeight) * 100;
  return scrollPercentage;
}
