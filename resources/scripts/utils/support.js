/**
 * Check if the browser support aspect-ratio.
 *
 * @returns {boolean} True if the browser support aspect-ratio and false otherwise.
 */
export const supportAspectRatio = () => {
  if (!window.CSS) {
    return false;
  }

  return CSS.supports('aspect-ratio: 16 / 9');
};
