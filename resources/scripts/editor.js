import domReady from '@roots/sage/client/dom-ready';

import { supportAspectRatio } from './utils/support';

/**
 * Application entrypoint
 */
domReady(async () => {
  const isSupportAspectRatio = supportAspectRatio();

  if (!isSupportAspectRatio) {
    document.documentElement.classList.add('browser-no-support');
    alert(
      'Your browser does not support aspect-ratio. Unfortunately that means you will not get the full experience from our website and miss out on cool features. We recommend you to update your browser to a modern version!',
    );
  }
});

/**
 * @see {@link https://webpack.js.org/api/hot-module-replacement/}
 */
if (import.meta.webpackHot) import.meta.webpackHot.accept(console.error);
