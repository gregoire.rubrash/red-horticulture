const NODE_ENV = process.env.NODE_ENV;
const IS_MOBILE =
  /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
    navigator.userAgent,
  ) ||
  ('MacIntel' === navigator.platform && navigator.maxTouchPoints > 1);

/**
 * Main environment variables.
 *
 * @var {Record<string, boolean>}
 */
export const ENV = Object.freeze({
  // Node environment
  NAME: NODE_ENV,
  IS_PROD: NODE_ENV === 'production',
  IS_DEV: NODE_ENV === 'development',

  // Device
  IS_DESKTOP: !IS_MOBILE,
  IS_MOBILE,
});
