import modular from 'modujs';

import * as modules from '../modules';

export default class Core {
  constructor() {
    this.app = new modular({
      modules: modules,
    });
  }

  init() {
    if (this.app) {
      this.app.init(this.app);
    }
  }
}
