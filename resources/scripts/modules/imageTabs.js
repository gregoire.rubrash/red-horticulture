import { module } from 'modujs';

export default class extends module {
  constructor(m) {
    super(m);

    this.events = {
      click: {
        button: 'toggleActiveProduct',
      },
    }
  }

  init() {
    this.toggleActiveImage(0);
    this.toggleActiveDescription(0);
    this.toggleActiveButton(0);
  }

  toggleActiveProduct(event) {
    const button = event.target.closest('button');
    const productId = button.getAttribute('data-product-id');

    this.toggleActiveImage(productId);
    this.toggleActiveDescription(productId);
    this.toggleActiveButton(productId);
  }

  toggleActiveDescription(productId) {
    const descriptions = this.$('description');

    // Masquer toutes les descriptions
    descriptions.forEach(description => {
      description.style.gridTemplateRows = '0fr'
    });

    const activeDescription = descriptions[productId];  
    activeDescription.style.gridTemplateRows = '1fr'
  }

  toggleActiveImage(productId) {
    const images = this.$('image');
    
    // Masquer toutes les images
    images.forEach(image => {
      image.classList.add('!hidden');
    });
    
    // Afficher l'image du produit sur lequel on a cliqué
    const activeImage = images[productId];
    activeImage.classList.remove('!hidden');
  }  

  toggleActiveButton(productId) {
    const buttons = this.$('button');
    // Masquer toutes les boutons
    buttons.forEach(button => {
      button.querySelector('img').classList.remove('rotate-180');
    });
    
    // Afficher l'image du produit sur lequel on a cliqué
    const activeButton = buttons[productId].querySelector('img');
    activeButton.classList.add('rotate-180');
  }  
}
