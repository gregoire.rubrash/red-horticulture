import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import { module } from 'modujs';

gsap.registerPlugin(ScrollTrigger);

export default class extends module {
  constructor(m) {
    super(m);
  }
  
  init() {
    this.offset = 2
    this.getElems()
    this.setHeight()
    this.handleResize()
    this.scrollTrigger()

    window.addEventListener('resize', this.handleResize.bind(this)) 
  }

  handleResize() {
    this.setPath()
    this.setWidth()
  }

  getElems() {
    this.blocks = document.querySelector('.blocks')
    this.timeline = document.getElementById('timeline')
    this.container = this.el.querySelector('.b-timeline__container')
    this.wrapper = this.el.querySelector('.b-timeline__wrapper')
    this.items = this.el.querySelectorAll('.b-timeline__item')
    this.circle = this.el.querySelector('.circle')
    this.path = this.el.querySelector('.path')
    this.itemsTitle = this.el.querySelectorAll('.b-timeline__item--title')
    this.titles = Array.from(this.itemsTitle)
  }

  setHeight() {
    const heights = this.titles.map(el => el.clientHeight)
    const maxHeight = Math.max(...heights)

    this.titles.forEach(el => {
      el.style.height = `${maxHeight}px`
    })
  }

  setWidth() {
    if (this.container.offsetWidth < 351) {
      this.offset = 0.25
      this.items.forEach(el => {
        el.style.width = `${this.container.offsetWidth - 5}px`
      })
    }
  }

  setPath() {
    const circlePositionTop = this.circle.offsetTop
    const circleHeight = this.circle.offsetHeight
    const circleCenter = circlePositionTop + circleHeight / 2
    this.path.style.top = `${circleCenter}px`
  }

  scrollTrigger() {
    let items = gsap.utils.toArray(this.items)
    
    gsap.to(items, {
      xPercent: -100 * (items.length - this.offset),
      ease: 'none',
      scrollTrigger: {
        trigger: this.timeline,
        pin: true,
        start: '-=100 +=150',
        endTrigger: 'bottom',
        end: () => "+=" + this.wrapper.offsetWidth,
        scrub: 1,
      },
    })
  }
}