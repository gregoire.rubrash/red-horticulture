import { module } from 'modujs';

export default class extends module {
  constructor(m) {
    super(m);

    this.events = {
      click: {
        button: 'handleClick',
      },
    }
  }

  destroy() {
    super.destroy();
  }

  handleClick() {
    const list = this.$('list')[0];
    if (!list) return;

    const isHidden = list.getAttribute('aria-hidden') === 'true';

    if (isHidden) {
      list.classList.remove('opacity-0');
      list.classList.remove('invisible');
    } else {
      list.classList.add('opacity-0');
      list.classList.add('invisible');
    }

    list.setAttribute('aria-hidden', !isHidden);
  }
}
