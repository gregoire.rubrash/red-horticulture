import { module } from 'modujs';
import Swiper from 'swiper';
import { Pagination } from 'swiper/modules';

export default class extends module {
  constructor(m) {
    super(m);
  }

  init() {
    const slider = this.$('slider')[0]

    new Swiper(slider, {
      loop: true,
      slidesPerView: 1,
      speed: 700,
      modules: [ Pagination ],
      pagination: {
          el: ".cas-producteurs-pagination",
          clickable: true,
      },
    })
  }

  destroy() {
    super.destroy();
  }
}
