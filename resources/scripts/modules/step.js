import { module } from 'modujs';
import Swiper from 'swiper';
import { Navigation } from 'swiper/modules';

export default class extends module {
  constructor(m) {
    super(m);
  }

  init() {
    this.handleScroll = this.handleScroll.bind(this)

    this.pathElements = Array.from(this.el.querySelectorAll('path'))
    this.pathDetails = []

    this.pathElements.forEach((path) => {
        if (path instanceof SVGGeometryElement) {
            const pathLength = path.getTotalLength()
            const pathRect = path.getBoundingClientRect()
            const pathTop = window.scrollY + pathRect.top
            const pathBottom = window.scrollY + pathRect.bottom
            const pathReverse = path.getAttribute('data-reverse') !== null

            this.pathDetails.push({
                length: pathLength,
                top: pathTop,
                bottom: pathBottom,
                reverse: pathReverse,
            })

            path.style.strokeDasharray = `${pathLength} ${pathLength}`
            path.style.strokeDashoffset = pathReverse ? '0' : `${pathLength}`
        }
    })

    window.addEventListener('scroll', this.handleScroll)

    this.createSwiper();
  }

  destroy() {
    super.destroy();

    window.removeEventListener('scroll', this.handleScroll)
  }

  createSwiper() {
    const slider = this.$('slider')[0]

    new Swiper(slider, {
      loop: true,
      speed: 1000,
      slidesPerView: 1,
      modules: [Navigation],
      navigation: {
          nextEl: ".parc-swiper-button-next",
          prevEl: ".parc-swiper-button-prev",
      },
    })
  }

  handleScroll() {
    const scrollY = window.scrollY
    const scrollMid = scrollY + window.innerHeight / 2

    this.pathElements.forEach((path, index) => {
      if (path instanceof SVGGeometryElement && index < this.pathDetails.length) {
        const { length, top, bottom, reverse } = this.pathDetails[index]

        let scrollPercentage = 0
        if (scrollMid < top) {
          scrollPercentage = 0
        } else if (scrollMid > bottom) {
          scrollPercentage = 1
        } else {
          scrollPercentage = (scrollMid - top) / (bottom - top)
        }

        const dashOffset = reverse ? length * scrollPercentage : length * (1 - scrollPercentage)

        // Utilisez requestAnimationFrame pour optimiser les performances de défilement
        requestAnimationFrame(() => {
          path.style.strokeDashoffset = `${dashOffset}`
        })
      }
    })
  }
}
