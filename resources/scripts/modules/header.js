import { module } from 'modujs';

export default class extends module {
  constructor(m) {
    super(m);

    this.events = {
      click: {
        burger: 'toggleMobileMenu',
      },
    }
  }

  init() {
    this.closeSubMenu = this.closeSubMenu.bind(this);
    this.toggleSubMenu = this.toggleSubMenu.bind(this);

    this.burger = this.$('burger')[0]
    this.logo = this.$('logo')[0]

    this.mobileNav = this.el.querySelector('#mobile-nav');
    this.subMenus = this.el.querySelectorAll('.menu-item-has-children');
    this.subSubMenus = this.el.querySelectorAll('.sub-menu-right');
    this.mobileNavGoBack = this.el.querySelector('#mobile-nav-go-back');
    this.blackMenuBackground = this.el.querySelector('#black-menu-background');
    this.solutionFullLed = this.el.querySelector('.full-led');

    this.previousChild = null;
    this.subChildren = [];

    this.blackMenuBackground.addEventListener('click', () => this.closeSubMenu());
    this.mobileNavGoBack.addEventListener('click', () => this.closeSubMenu());
    this.subMenus.forEach(subMenu => {
      subMenu.addEventListener('click', (event) => this.toggleSubMenu(event, subMenu));
    });
    this.subSubMenus.forEach(subSubMenu => {
      this.duplicateSubSubMenu(subSubMenu);
      subSubMenu.addEventListener('mouseenter', (event) => {
        if (!subSubMenu.querySelector('.sub-menu').classList.contains('sub-menu--open')) {
          this.toggleSubMenu(event, subSubMenu)
        }
      });
      subSubMenu.addEventListener('click', (event) => {
        this.toggleSubMenu(event, subSubMenu)
      });
    });
    this.solutionFullLed.addEventListener('mouseenter', () => this.removeActiveSubSubMenus(this))
  }

  destroy() {
    super.destroy();
  }

  toggleBurger() {
    this.burger.classList.toggle('is-open')
  }

  toggleMobileMenu() {
    this.toggleBurger();
    this.mobileNav.classList.toggle('-translate-x-full');
    this.subMenus.forEach(subMenu => {
      subMenu.querySelector('.sub-menu').classList.remove('sub-menu--open');
    })
    this.logo.classList.remove('invisible');
    this.logo.classList.add('visible');
    this.mobileNavGoBack.classList.remove('opacity-100');
    this.mobileNavGoBack.classList.add('opacity-0');
  }

  toggleSubMenu(event, subMenu) {
    event.stopPropagation();

    if (!subMenu.classList.contains('produits-et-services') && !subMenu.classList.contains('sub-menu-right')) {
      this.subChildren = subMenu.querySelectorAll('.sub-menu .menu-item a');
      this.eventSubmenu();
    }

    this.logo.classList.add('invisible');
    this.logo.classList.remove('visible');
    this.mobileNavGoBack.classList.add('opacity-100');
    this.mobileNavGoBack.classList.remove('opacity-0');

    if (!subMenu.classList.contains('sub-menu-right')) {
      this.removeActiveSubMenus(subMenu);
    } else {
      this.removeActiveSubSubMenus(subMenu);
    }
    subMenu.querySelector('.sub-menu').classList.toggle('sub-menu--open');
    subMenu.classList.toggle('menu-item-has-children--open');
    if (subMenu.querySelector('.sub-menu').classList.contains('sub-menu--open')) {
      this.blackMenuBackground.classList.add('black-menu-background--open')
    } else {
      this.blackMenuBackground.classList.remove('black-menu-background--open')
    }
  }

  closeSubMenu() {
    this.subMenus.forEach(subMenu => {
      subMenu.querySelector('.sub-menu').classList.remove('sub-menu--open');
      subMenu.classList.remove('menu-item-has-children--open');
    });
    this.logo.classList.add('visible');
    this.logo.classList.remove('invisible');
    this.mobileNavGoBack.classList.remove('opacity-100');
    this.mobileNavGoBack.classList.add('opacity-0');
    this.blackMenuBackground.classList.remove('black-menu-background--open')
  }

  removeActiveSubMenus(subMenu) {
    this.subMenus.forEach(singleSubMenu => {
      if (singleSubMenu != subMenu) {
        singleSubMenu.querySelector('.sub-menu').classList.remove('sub-menu--open');
        singleSubMenu.classList.remove('menu-item-has-children--open');
      }
    })
  }

  removeActiveSubSubMenus(subMenu) {
    this.subMenus.forEach(singleSubMenu => {
      if (singleSubMenu != subMenu && singleSubMenu.classList.contains('sub-menu-right')) {
        singleSubMenu.querySelector('.sub-menu').classList.remove('sub-menu--open');
      }
    })
  }
  eventSubmenu() {
    if (this.subChildren) {
      this.previousChild = this.subChildren[0];
      this.previousChild.classList.add('active');
      this.previousChild.style.setProperty('--opacity', '1');
      this.subChildren.forEach(subChild => {
        subChild.addEventListener('mouseover', () => {
          this.toggleSubmenuChild(subChild);
        })
      })
    }
  }

  toggleSubmenuChild(subChild) {
    if (this.previousChild === subChild) {
      return;
    }

    subChild.classList.add('active');
    subChild.style.setProperty('--opacity', '1');
    this.previousChild.classList.remove('active');
    this.previousChild.style.setProperty('--opacity', '0');

    this.previousChild = subChild;
  }

  duplicateSubSubMenu(subSubMenu) {
    const elementToDuplicate = subSubMenu.querySelector('.sub-menu');
    // Cloner l'élément
    const clonedElement = elementToDuplicate.cloneNode(true);
    clonedElement.classList.remove('sub-menu');
    clonedElement.classList.add('also-sub-menu');

    // Insérer le clone juste après l'élément original
    elementToDuplicate.insertAdjacentElement('afterend', clonedElement);
  }
}
