export { default as Sample } from './modules/sample';
export { default as Lang } from './modules/lang';
export { default as LatestPostsSlider } from './modules/latest-posts-slider';
export { default as ImageTabs } from './modules/imageTabs';
export { default as Header } from './modules/header';
export { default as Step } from './modules/step';
export { default as Timeline } from './modules/timeline';
