<?php

namespace App\Helpers;

class Thumbnail {
    public static function get(?int $postID, ?string $size = 'large') {
        if (empty($postID)) {
            return null;
        }

        $thumbnail = null;
        if (\has_post_thumbnail($postID)) {
            $thumbnailID = \get_post_thumbnail_id($postID);
            $thumbnailAttachment = \wp_get_attachment_image_src($thumbnailID, $size);
            $thumbnail = (object) [
                'src' => $thumbnailAttachment[0],
                'alt' => \get_post_meta($thumbnailID, '_wp_attachment_image_alt', true),
                'sizes' => [],
            ];
        }

        return $thumbnail;
    }
}
