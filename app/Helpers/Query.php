<?php

namespace App\Helpers;

class Query {
    public static function get(?string $keyName = 'q') {
        if (isset($_GET[$keyName])) {
            $query = $_GET[$keyName];
            $query = esc_attr($query);
            return $query;
        }

        return '';
    }
}
