<?php

namespace App;

add_action('init', function () {
  pll_register_string('rubrash', 'Contactez-nous', 'true');
  pll_register_string('rubrash', 'Retour', 'true');
  pll_register_string('rubrash', 'Lire l\'article', 'true');
  pll_register_string('rubrash', 'En savoir plus', 'true');
  pll_register_string('rubrash', 'Recherche', 'true');
  pll_register_string('rubrash', 'Rechercher', 'true');
  pll_register_string('rubrash', 'Rejoindre l\'équipe', 'true');
  pll_register_string('rubrash', 'Nos postes', 'true');
  pll_register_string('rubrash', 'Postuler', 'true');
  pll_register_string('rubrash', 'Aucun poste disponible pour le moment', 'true');
  pll_register_string('rubrash', 'Rechercher un cas producteur', 'true');
  pll_register_string('rubrash', 'Aucun cas producteurs ne correspond à votre recherche.', 'true');
  pll_register_string('rubrash', 'Aucun articles ne correspond à votre recherche.', 'true');
  pll_register_string('rubrash', 'Vous postulez !', 'true');
  pll_register_string('rubrash', 'Afficher le menu', 'true');
  pll_register_string('rubrash', 'Aller vers le contenu', 'true');
  pll_register_string('rubrash', 'champs obligatoires', 'true');
  pll_register_string('rubrash', 'Page introuvable', 'true');
  pll_register_string('rubrash', 'Mais rassurez vous tout espoir n\'est pas perdu', 'true');
});