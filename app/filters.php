<?php

/**
 * Theme filters.
 */

namespace App;

/**
 * Add "… Continued" to the excerpt.
 *
 * @return string
 */
add_filter('excerpt_more', function () {
    return sprintf(' &hellip; <a href="%s">%s</a>', get_permalink(), __('Continued', 'sage'));
});

/**
 * Save ACF fields to JSON locally.
 *
 * @return string
 */
add_filter('acf/settings/save_json', function (): string {
    $acfDir = get_stylesheet_directory() . '/acf-json';
    if (!is_dir($acfDir)) {
        mkdir($acfDir);
    }
    return $acfDir;
});

/**
 * Disable redirect guessing
 */
add_filter('do_redirect_guess_404_permalink', '__return_false');

/**
 * Disable wp-json users access
 */
add_filter('rest_endpoints', function ($endpoints) {
    if (isset($endpoints['/wp/v2/users'])) {
        unset($endpoints['/wp/v2/users']);
    }
    if (isset($endpoints['/wp/v2/users/(?P<id>[\d]+)'])) {
        unset($endpoints['/wp/v2/users/(?P<id>[\d]+)']);
    }
    return $endpoints;
});

/**
 * Hide login hints
 */
add_filter('login_errors', function () {
    return __('<strong>Erreur</strong> : une des informations mentionnée est incorrecte.', 'rubrash');
});

/**
 * Allow only ACF blocks.
 */
add_filter('allowed_block_types_all', function () {
    $allowedBlocks = [];

    // Add all ACF custom blocks
    $acfBlocks = acf_get_block_types();

    foreach ($acfBlocks as $acfBlock) {
        $allowedBlocks[] = $acfBlock['name'];
    }

    return $allowedBlocks;
});
