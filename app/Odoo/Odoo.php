<?php

namespace App\Odoo;

use function Roots\add_actions;

/**
 * Connect to the Odoo API.
 *
 * @author Grégoire Ciles <bonjour@gregoireciles.fr>
 */
class Odoo
{
    private $credentials = null;
    private $api = [];
    private $client;
    private $models;
    private $uid;
    private $model = 'res.partner';
    private $companyID = null;

    public function __construct()
    {
        $this->credentials = get_field('odoo_credentials', 'options');
        $credentialsIsInvalid = $this->credentialsIsInvalid();
        if ($credentialsIsInvalid) {
            return;
        }

        $this->api = (object) [
            'url' => $this->credentials['odoo_url'],
            'db' => $this->credentials['odoo_db'],
            'email' => $this->credentials['odoo_email'],
            'password' => $this->credentials['odoo_password'],
        ];

        $this->client = \ripcord::client($this->api->url . "/xmlrpc/2/common");
        $this->models = \ripcord::client($this->api->url . "/xmlrpc/2/object");
        $this->connect();
    }

    private function credentialsIsInvalid()
    {
        if (
            empty($this->credentials) ||
            empty($this->credentials['odoo_url']) ||
            empty($this->credentials['odoo_db']) ||
            empty($this->credentials['odoo_email']) ||
            empty($this->credentials['odoo_password'])
        ) {
            return true;
        }

        return false;
    }

    private function connect()
    {
        $response = $this->client->authenticate($this->api->db, $this->api->email, $this->api->password, []);

        if (empty($response)) {
            throw new \Exception('Unable to authenticate with Odoo.');
        }

        if (is_array($response) && array_key_exists('faultString', $response)) {
            throw new \Exception('Unable to authenticate with Odoo.' . PHP_EOL . $response['faultString']);
        }

        $this->uid = $response;
    }

    /**
     * Add a company to Odoo if it does not exist, or get its ID if it does.
     *
     * @return self
     */
    public function addOrGetCompany(string $name): self
    {
        $existingCompany = $this->models->execute_kw(
            $this->api->db,
            $this->uid,
            $this->api->password,
            $this->model,
            'search_read',
            [[
                ['name', '=', $name],
                ['is_company', '=', true],
            ]],
            ['fields' => ['id']]
        );

        if (!empty($existingCompany)) {
            $this->companyID = $existingCompany[0]['id'];
        } else {
            $this->companyID = $this->models->execute_kw(
                $this->api->db,
                $this->uid,
                $this->api->password,
                $this->model,
                'create',
                [[
                    'name' => $name,
                    'is_company' => true,
                ]]
            );
        }

        return $this;
    }

    /**
     * Add a contact to Odoo.
     *
     * @return self
     */
    public function addContact(object $contact): self
    {
        $contactFormatted = [
            'name' => $contact->name,
            'email' => $contact->email
        ];

        if (!empty($contact->phone)) {
            $contactFormatted['phone'] = $contact->phone;
        }

        if (!empty($contact->city)) {
            $contactFormatted['city'] = $contact->city;
        }

        if (!empty($this->companyID)) {
            $contactFormatted['parent_id'] = $this->companyID;
        }

        if ($this->contactNotExists($contact)) {
            $this->models->execute_kw(
                $this->api->db,
                $this->uid,
                $this->api->password,
                $this->model,
                'create',
                [$contactFormatted]
            );
        }

        return $this;
    }

    /**
     * Check if a contact already exists in Odoo.
     *
     * @return bool True if the contact does not exist, false otherwise.
     */
    private function contactNotExists(object $contact): bool
    {
        $haystack = [
            ['name', '=', $contact->name],
            ['email', '=', $contact->email],
            ['is_company', '=', false],
        ];

        if (!empty($this->companyID)) {
            $haystack[] = ['parent_id', '=', $this->companyID];
        }

        $existingContact = $this->models->execute_kw(
            $this->api->db,
            $this->uid,
            $this->api->password,
            $this->model,
            'search_read',
            [$haystack],
            ['fields' => ['id']]
        );

        return empty($existingContact);
    }
}
