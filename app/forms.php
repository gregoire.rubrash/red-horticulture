<?php

namespace App;

use App\Odoo\Odoo;

/**
 * Format form fields into an array of objects.
 *
 * Iterates through the form fields and formats them into an array of objects,
 * each containing the field's ID, name, and value.
 *
 * @param array $form  The form array containing the fields.
 * @param array $entry The form entry data.
 *
 * @return array Returns an array of formatted field objects.
 */
function formatFormFields($form, $entry) {
    $fieldsFormatted = [];

    foreach ($form['fields'] as $field) {
        $inputs = $field->get_entry_inputs();
        if (!is_array($inputs)) {
            $value = rgar($entry, (string) $field->id);
            $fieldsFormatted[$field->id] = (object) [
                'id' => $field->id,
                'name' => $field->label,
                'value' => $value,
            ];
        }
    }

    return $fieldsFormatted;
}

/**
 * Handle `Contact` Forms submissions and send them to Odoo.
 */
add_action('gform_after_submission_1', function ($entry, $form) {
    $fieldsFormatted = formatFormFields($form, $entry);

    $contact = (object) [
        'name' => $fieldsFormatted[1]->value . ' ' . $fieldsFormatted[3]->value,
        'city' => $fieldsFormatted[6]->value,
        'email' => $fieldsFormatted[12]->value,
        'phone' => $fieldsFormatted[13]->value,
    ];

    $odoo = new Odoo();
    $odoo
        ->addOrGetCompany($fieldsFormatted[7]->value)
        ->addContact($contact);
}, 10, 2);

/**
 * Handle `Career` Forms submissions and send them to Odoo.
 */
add_action('gform_after_submission_2', function ($entry, $form) {
    $fieldsFormatted = formatFormFields($form, $entry);

    $contact = (object) [
        'name' => $fieldsFormatted[1]->value . ' ' . $fieldsFormatted[3]->value,
        'city' => $fieldsFormatted[6]->value,
        'email' => $fieldsFormatted[4]->value,
        'phone' => $fieldsFormatted[5]->value,
    ];

    $odoo = new Odoo();
    $odoo->addContact($contact);
}, 10, 2);

/**
 * Handle `Assistance` Forms submissions and send them to Odoo.
 */
add_action('gform_after_submission_3', function ($entry, $form) {
    $fieldsFormatted = formatFormFields($form, $entry);

    $contact = (object) [
        'name' => $fieldsFormatted[1]->value . ' ' . $fieldsFormatted[3]->value,
        'email' => $fieldsFormatted[8]->value,
        'phone' => $fieldsFormatted[9]->value,
    ];

    $odoo = new Odoo();
    $odoo
        ->addOrGetCompany($fieldsFormatted[6]->value)
        ->addContact($contact);
}, 10, 2);

/**
 * Handle `Press Kit` Forms submissions and send them to Odoo.
 */
add_action('gform_after_submission_4', function ($entry, $form) {
    $fieldsFormatted = formatFormFields($form, $entry);

    $contact = (object) [
        'name' => $fieldsFormatted[1]->value . ' ' . $fieldsFormatted[3]->value,
        'email' => $fieldsFormatted[8]->value,
    ];

    $odoo = new Odoo();
    $odoo->addContact($contact);
}, 10, 2);

/**
 * Handle `Newsletter` Forms submissions and send them to Odoo.
 */
add_action('gform_after_submission_6', function ($entry, $form) {
    $fieldsFormatted = formatFormFields($form, $entry);

    $contact = (object) [
        'name' => $fieldsFormatted[1]->value . ' ' . $fieldsFormatted[3]->value,
        'email' => $fieldsFormatted[4]->value,
    ];

    $odoo = new Odoo();
    $odoo->addContact($contact);
}, 10, 2);
