<?php

namespace App;

/**
 * App Root
 *
 * @var string
 */
define('APP_ROOT', dirname(__FILE__));

/**
 * Verify if odoo credentials are set in the theme settings
 * and display an error message if not.
 */
add_action('init', function () {
    $odooCredentials = get_field('odoo_credentials', 'options');
    $errorMessage = '';

    if (empty($odooCredentials)) {
        $errorMessage .= '<b>Odoo credentials</b> are missing.';
    } else {
        $missingFields = [];

        if (!array_key_exists('odoo_url', $odooCredentials) || empty($odooCredentials['odoo_url'])) {
            $missingFields[] = 'URL';
        }

        if (!array_key_exists('odoo_db', $odooCredentials) || empty($odooCredentials['odoo_db'])) {
            $missingFields[] = 'Database';
        }

        if (!array_key_exists('odoo_email', $odooCredentials) || empty($odooCredentials['odoo_email'])) {
            $missingFields[] = 'Email';
        }

        if (!array_key_exists('odoo_password', $odooCredentials) || empty($odooCredentials['odoo_password'])) {
            $missingFields[] = 'Password';
        }

        if (!empty($missingFields)) {
            $errorMessage = '<b>Odoo';
            if (count($missingFields) > 1) {
                $errorMessage .= ' (' . implode(', ', $missingFields) . ')';
            } else {
                $errorMessage .= ' ' . $missingFields[0];
            }
            $errorMessage .= '</b> is missing.';
        }
    }

    if (!empty($errorMessage)) {
        add_action('admin_notices', function () use ($errorMessage) {
            $class   = 'notice notice-error';
            $message = __(
                '<b>Error:</b> ' . $errorMessage . ' Please add them in the <a href="/wp-admin/admin.php?page=theme-settings">Theme Settings</a>.',
                'rubrash'
            );
            printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), wp_kses_post($message));
        });
    }
});

if (file_exists(APP_ROOT . '/Ripcord/ripcord.php')) {
    require_once APP_ROOT . '/Ripcord/ripcord.php';
}
