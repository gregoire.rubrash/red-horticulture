<?php

/**
 * Theme CPT and taxonomies.
 */

namespace App;

/**
 * Custom post type "cas-producteur"
 */
add_action('init', function () {
  $labels = array(
    'name' => _x('Cas producteurs', 'post type general name'),
    'singular_name' => _x('Cas producteur', 'post type singular name'),
    'menu_name' => 'Cas producteurs'
  );
  $args = array(
    'labels'             => $labels,
    'description'        => 'Liste des cas producteurs',
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'cas-producteur' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 5,
    'menu_icon'          => 'dashicons-groups',
    'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
    'taxonomies'         => array( 'categorieCasProducteurs' ),
    'show_in_rest'       => true
  );

  register_post_type('cas-producteur', $args);
});


/**
 * Custom post type "actualite"
 */
add_action('init', function () {
  $labels = array(
    'name' => _x('Actualités', 'post type general name'),
    'singular_name' => _x('Actualité', 'post type singular name'),
    'menu_name' => 'Actualités'
  );
  $args = array(
    'labels' => $labels,
    'description' => 'Liste des actualités',
    'menu_icon' => 'dashicons-groups',
    'public' => true,
    'show_in_rest' => true,
    'taxonomies' => array('categorieActualite'),
    'publicly_queryable' => true,
    'menu_position' => 5,
    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'has_archive' => true,
  );
  register_post_type('actualite', $args);
});


/**
 * Custom post type "evenement"
 */
add_action('init', function () {
  $labels = array(
    'name' => _x('Évènements', 'post type general name'),
    'singular_name' => _x('Évènement', 'post type singular name'),
    'menu_name' => 'Évènements'
  );
  $args = array(
    'labels' => $labels,
    'description' => 'Liste des évènements',
    'menu_icon' => 'dashicons-groups',
    'public' => true,
    'show_in_rest' => true,
    'taxonomies' => array('categorieEvenement'),
    'publicly_queryable' => true,
    'menu_position' => 5,
    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'has_archive' => true,
  );
  register_post_type('evenement', $args);
});

/**
 * Custom post type "document"
 */
add_action('init', function () {
  $labels = array(
    'name' => _x('Documents', 'post type general name'),
    'singular_name' => _x('Document', 'post type singular name'),
    'menu_name' => 'Documents'
  );
  $args = array(
    'labels' => $labels,
    'description' => 'Liste des documents',
    'menu_icon' => 'dashicons-groups',
    'public' => true,
    'show_in_rest' => true,
    'publicly_queryable' => true,
    'menu_position' => 5,
    'supports' => array( 'title', 'editor'),
    'has_archive' => true,
  );
  register_post_type('document', $args);
});


/**
 * Custom post type "outil"
 */
add_action('init', function () {
  $labels = array(
    'name' => _x('Outils', 'post type general name'),
    'singular_name' => _x('Outil', 'post type singular name'),
    'menu_name' => 'Outils'
  );
  $args = array(
    'labels' => $labels,
    'description' => 'Liste des outils',
    'menu_icon' => 'dashicons-groups',
    'public' => true,
    'show_in_rest' => true,
    'publicly_queryable' => true,
    'menu_position' => 5,
    'supports' => array( 'title', 'editor'),
    'has_archive' => true,
  );
  register_post_type('outil', $args);
});

/**
 * Add custom taxonomies
 *
 * Additional custom taxonomies can be defined here
 * https://codex.wordpress.org/Function_Reference/register_taxonomy
 */
add_action('init', function () {
  // Add new "Catégories" taxonomy to cas producteurs
  register_taxonomy(
    'categorieCasProducteurs',
    'cas-producteurs',
    array(
      'show_in_rest' => true,
      'show_admin_column' => true,
      'meta_box_cb' => false,
      // This array of options controls the labels displayed in the WordPress Admin UI
      'labels' => array(
        'name' => _x('Catégorie', 'taxonomy general name'),
        'singular_name' => _x('Catégorie', 'taxonomy singular name'),
        'search_items' => __('Chercher des catégories'),
        'all_items' => __('Toutes les catégories'),
        'parent_item' => __('Parent Catégorie'),
        'parent_item_colon' => __('Parent Catégorie:'),
        'edit_item' => __('Edit Catégorie'),
        'update_item' => __('Update Catégorie'),
        'add_new_item' => __('Add New Catégorie'),
        'new_item_name' => __('New Catégorie Name'),
        'menu_name' => __('Catégories'),
      ),
      // Control the slugs used for this taxonomy
      'rewrite' => array(
        'slug' => 'categories',
        // This controls the base slug that will display before each term
        'with_front' => false,
        // Don't display the category base before "/locations/"

      )
    )
  );

  // Add new "Catégories" taxonomy to "Actualités"
  register_taxonomy(
    'categorieActualites',
    'actualite',
    array(
      'show_in_rest' => true,
      'show_admin_column' => true,
      'meta_box_cb' => false,
      // This array of options controls the labels displayed in the WordPress Admin UI
      'labels' => array(
        'name' => _x('Catégorie', 'taxonomy general name'),
        'singular_name' => _x('Catégorie', 'taxonomy singular name'),
        'search_items' => __('Chercher des catégories'),
        'all_items' => __('Toutes les catégories'),
        'parent_item' => __('Parent Catégorie'),
        'parent_item_colon' => __('Parent Catégorie:'),
        'edit_item' => __('Edit Catégorie'),
        'update_item' => __('Update Catégorie'),
        'add_new_item' => __('Add New Catégorie'),
        'new_item_name' => __('New Catégorie Name'),
        'menu_name' => __('Catégories'),
      ),
      // Control the slugs used for this taxonomy
      'rewrite' => array(
        'slug' => 'categories',
        // This controls the base slug that will display before each term
        'with_front' => false,
        // Don't display the category base before "/locations/"

      )
    )
  );

  // Add new "Catégories" taxonomy to cas producteurs
  register_taxonomy(
    'categorieEvenements',
    'evenement',
    array(
      'show_in_rest' => true,
      'show_admin_column' => true,
      'meta_box_cb' => false,
      // This array of options controls the labels displayed in the WordPress Admin UI
      'labels' => array(
        'name' => _x('Catégorie', 'taxonomy general name'),
        'singular_name' => _x('Catégorie', 'taxonomy singular name'),
        'search_items' => __('Chercher des catégories'),
        'all_items' => __('Toutes les catégories'),
        'parent_item' => __('Parent Catégorie'),
        'parent_item_colon' => __('Parent Catégorie:'),
        'edit_item' => __('Edit Catégorie'),
        'update_item' => __('Update Catégorie'),
        'add_new_item' => __('Add New Catégorie'),
        'new_item_name' => __('New Catégorie Name'),
        'menu_name' => __('Catégories'),
      ),
      // Control the slugs used for this taxonomy
      'rewrite' => array(
        'slug' => 'categories',
        // This controls the base slug that will display before each term
        'with_front' => false,
        // Don't display the category base before "/locations/"

      )
    )
  );
}, 0);
