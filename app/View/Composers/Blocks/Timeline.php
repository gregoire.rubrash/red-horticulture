<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class Timeline extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.timeline',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'title' => $this->title(),
            'content' => $this->content(),
            'choice_center' => $this->choiceCenter(),
            'timeline' => $this->timeline(),
        ];
    }

    public function title(): ?string
    {
        return get_field('title');
    }

    public function content(): ?string
    {
        return get_field('content');
    }

    public function choiceCenter()
    {
        return get_field('choice_center');
    }

    public function timeline()
    {
        return get_field('repeater_timeline');
    }
}
