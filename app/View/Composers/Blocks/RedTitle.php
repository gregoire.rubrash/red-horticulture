<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class RedTitle extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.title',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'level' => $this->level(),
            'content' => $this->content(),
            'position' => $this->position(),
        ];
    }

    public function level(): int
    {
        return get_field('level');
    }

    public function content(): string
    {
        return get_field('content');
    }

    public function position(): string
    {
        return get_field('position');
    }
}
