<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class HighlightContent extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.highlight-content',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
          'image' => $this->image(),
          'content' => $this->content(),
          'title' => $this->title(),
          'repeaterContent' => $this->repeaterContent(),
        ];
    }

    public function image()
    {
        return get_field('image');
    }

    public function content()
    {
        return get_field('content');
    }

    public function title()
    {
        return get_field('title');
    }

    public function repeaterContent()
    {
        return get_field('repeater_content');
    }
}
