<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class Phone extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.phone',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'background' => $this->background(),
            'image' => $this->image(),
            'title' => $this->title(),
            'showLinks' => $this->showLinks(),
            'linksDL' => $this->linksDL(),
            'text' => $this->text(),
            'link' => $this->link(),
        ];
    }

    public function background(): ?string {
        $background = get_field('background');
        if (empty($background)) {
            return null;
        }

        return $background;
    }

    public function image(): ?object
    {
        $image = get_field('image');
        if (empty($image)) {
            return null;
        }

        return (object) [
            'url' => $image['url'],
            'alt' => $image['alt'],
            'sizes' => $image['sizes'],
        ];
    }

    public function title(): string
    {
        return get_field('title');
    }

    public function showLinks(): bool
    {
        return get_field('show_links');
    }

    public function linksDL(): array
    {
        $list = collect(get_field('list_links_dl'));

        if ($list->isEmpty()) {
            return [];
        }

        return $list->map(function ($item) {
            $image = $item['image'];
            if (empty($image)) {
                return null;
            }

            $link = $item['link'];
            if (empty($link)) {
                return null;
            }

            return (object) [
                'link' => (object) $link,
                'image' => (object) [
                    'url' => $image['url'],
                    'alt' => $image['alt'],
                    'sizes' => $image['sizes'],
                ],
            ];
        })->filter()->all();
    }

    public function text(): ?string
    {
        return get_field('text');
    }

    public function link(): ?array
    {
        return get_field('link');
    }
}
