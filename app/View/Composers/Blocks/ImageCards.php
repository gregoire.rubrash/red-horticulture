<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class ImageCards extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.image-cards',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'title' => $this->title(),
            'cards' => $this->cards(),
        ];
    }

    public function title(): ?string
    {
        return get_field('title');
    }

    public function cards(): ?array
    {
        return get_field('repeater_cards');
    }
}
