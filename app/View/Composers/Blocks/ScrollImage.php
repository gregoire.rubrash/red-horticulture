<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class ScrollImage extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.scroll-image',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'images' => $this->images(),
            'title' => $this->title(),
            'content' => $this->content(),
            'repeaterContent' => $this->repeaterContent(),
            'list' => $this->list(),
            'dark' => $this->dark(),
            'choice_side' => $this->choiceSide(),
        ];
    }

    public function images()
    {
        return get_field('images');
    }

    public function title(): string
    {
        return get_field('title');
    }

    public function content(): string
    {
        return get_field('content');
    }

    public function repeaterContent()
    {
        return get_field('repeater_content');
    }

    public function list()
    {
        return get_field('list');
    }

    public function dark(): bool
    {
        return get_field('dark');
    }

    public function choiceSide(): bool
    {
        return get_field('choice_side');
    }
}
