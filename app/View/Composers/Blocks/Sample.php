<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class Sample extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.sample',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'myVar' => $this->myVar(),
        ];
    }

    public function myVar(): string
    {
        return get_field('my_variable');
    }
}
