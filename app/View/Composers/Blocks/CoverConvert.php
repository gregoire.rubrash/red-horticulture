<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;
use App\Helpers\Thumbnail;

class CoverConvert extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.cover-convert',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'image' => $this->image(),
            'title' => $this->title(),
            'content' => $this->content(),
            'link' => $this->link(),
            'id_form' => $this->idForm(),
        ];
    }

    public function image()
    {
        return Thumbnail::get(get_the_ID(), 'full');
    }

    public function title(): ?string
    {
        return get_field('title');
    }

    public function content(): ?string
    {
        return get_field('content');
    }

    public function link()
    {
        return get_field('link');
    }

    public function idForm()
    {
        return get_field('id_form');
    }
}
