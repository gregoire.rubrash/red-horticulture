<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class RedList extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.red-list',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'title' => $this->title(),
            'list' => $this->list(),
            'content_bottom' => $this->contentBottom(),
        ];
    }

    public function title() : ?string
    {
        return get_field('title');
    }

    public function list() : ?array
    {   
        return get_field('list_clone');
    }

    public function contentBottom() : ?string
    {
        return get_field('content_bottom');
    }
}
