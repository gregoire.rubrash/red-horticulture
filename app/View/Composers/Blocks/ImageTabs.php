<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class ImageTabs extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.image-tabs',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'title' => $this->title(),
            'products' => $this->products(),
        ];
    }

    public function title(): ?string
    {
        return get_field('title');
    }

    public function products()
    {
        return get_field('products');
    }
}
