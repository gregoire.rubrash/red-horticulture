<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class ContactButton extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.contact-button',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'position' => $this->position(),
            'link' => $this->link(),
        ];
    }

    public function position()
    {
        return get_field('position');
    }

    public function link()
    {
        return get_field('link_contact', 'options');
    }
}
