<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class ImagesGrid extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.images-grid',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'title' => $this->title(),
            'images' => $this->images(),
        ];
    }

    public function title(): ?string
    {
        return get_field('title');
    }

    public function images()
    {
        return get_field('repeater_images');
    }
}
