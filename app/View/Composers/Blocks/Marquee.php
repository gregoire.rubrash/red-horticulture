<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class Marquee extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.marquee',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'title' => $this->title(),
            'text' => $this->text(),
            'marquees' => $this->marquees(),
        ];
    }

    public function title(): string
    {
        return get_field('title');
    }

    public function text(): string
    {
        return get_field('text');
    }

    public function marquees(): array
    {
        $marquees = collect(get_field('list_marquee'));

        if ($marquees->isEmpty()) {
            return [];
        }

        return $marquees->map(function ($marquee) {
            return $this->sanitizeMarquee((object) $marquee);
        })->filter()->all();
    }

    private function sanitizeMarquee(object $marquee) {
        $marqueeImages = collect($marquee->marquee_images);

        if ($marqueeImages->isEmpty()) {
            return [];
        }

        return $marqueeImages->map(function ($item) {
            $image = $item['marquee_image'];
            if (empty($image)) {
                return null;
            }

            return (object) [
                'url' => $image['url'],
                'alt' => $image['alt'],
                'sizes' => $image['sizes'],
            ];
        })->filter()->all();
    }
}
