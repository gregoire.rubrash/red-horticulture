<?php

namespace App\View\Composers\Blocks;

use App\Helpers\Thumbnail;
use Roots\Acorn\View\Composer;

class Forms extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.forms',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'image' => $this->image(),
            'title' => $this->title(),
            'content_form' => $this->content(),
            'id_form' => $this->idForm(),
        ];
    }

    public function image()
    {
        return Thumbnail::get(get_the_ID(), 'full');
    }

    public function title()
    {
        return get_field('title');
    }

    public function content()
    {
        return get_field('content_form');
    }

    public function idForm()
    {
        return get_field('id');
    }
}
