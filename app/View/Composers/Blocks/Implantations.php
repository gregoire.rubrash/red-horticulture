<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class Implantations extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.implantations',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'suptitle' => $this->suptitle(),
            'title' => $this->title(),
            'implantations' => $this->implantations(),
            'image' => $this->image(),
        ];
    }

    public function suptitle(): ?string
    {
        return get_field('suptitle');
    }
    
    public function title(): ?string
    {
      return get_field('title');
    }

    public function implantations(): ?array
    {
      return get_field('implantations');
    }
    
    public function image(): ?array
    {
        return get_field('image');
    }
}
