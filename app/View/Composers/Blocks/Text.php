<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class Text extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.text',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'text' => $this->text(),
        ];
    }

    public function text()
    {
        return get_field('text');
    }
}
