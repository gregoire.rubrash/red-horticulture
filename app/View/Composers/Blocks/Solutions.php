<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class Solutions extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.solutions',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'title' => $this->title(),
            'text' => $this->text(),
            'list' => $this->list(),
            'link' => $this->link(),
        ];
    }

    public function title(): string
    {
        return get_field('title');
    }

    public function text(): string
    {
        return get_field('text');
    }

    public function list(): array
    {
        $list = collect(get_field('list'));

        if ($list->isEmpty()) {
            return [];
        }

        return $list->map(function ($item) {
            $imageTitle = $item['image_title'];
            if (empty($imageTitle)) {
                return null;
            }

            $image = $item['image'];
            if (empty($image)) {
                return null;
            }

            $link = $item['lien'];
            if (empty($link)) {
                return null;
            }

            return (object) [
                'title' => $imageTitle,
                'link' => $link,
                'image' => (object) [
                    'url' => $image['url'],
                    'alt' => $image['alt'],
                    'sizes' => $image['sizes'],
                ],
            ];
        })->filter()->all();
    }

    public function link(): array
    {
        return get_field('link');
    }
}
