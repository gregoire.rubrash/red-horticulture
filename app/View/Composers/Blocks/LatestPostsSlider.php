<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class LatestPostsSlider extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.latest-posts-slider',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'postType' => $this->postType(),
            'quantity' => $this->quantity(),
            'posts' => $this->posts(),
        ];
    }

    public function postType()
    {
        $postType = get_field('postType');

        switch ($postType) {
            case 'Post':
                $postType = 'post';
                break;
            case 'Cas producteur':
                $postType = 'cas-producteur';
                break;
            case 'Évènement':
                $postType = 'evenement';
                break;
            default:
                $postType = 'post';
                break;
        }

        return $postType;
    }

    public function quantity()
    {
        return get_field('quantity');
    }

    public function posts()
    {
        $postType = $this->postType();
        $quantity = $this->quantity();

        $posts = get_posts([
            'post_type' => $postType,
            'posts_per_page' => $quantity,
            'order' => 'DESC',
            'orderby' => 'date',
        ]);

        return $posts;
    }
}
