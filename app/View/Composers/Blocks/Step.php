<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class Step extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.step',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'choice_lines' => $this->choiceLines(),
            'steps' => $this->steps(),
        ];
    }

    public function choiceLines(): ?bool
    {
        return get_field('choice_lines');
    }

    public function steps(): mixed
    {
        return get_field('steps');
    }
}
