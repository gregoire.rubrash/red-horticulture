<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class ContentLink extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.content-link',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'title' => $this->title(),
            'content' => $this->content(),
            'link' => $this->link(),
        ];
    }

    public function title(): ?string
    {
        return get_field('title');
    }

    public function content(): ?string
    {
        return get_field('content');
    }

    public function link()
    {
        return get_field('link');
    }
}
