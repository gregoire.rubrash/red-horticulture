<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class FlexibleContent extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.flexible-content',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'image' => $this->image(),
            'double_image' => $this->double_image(),
            'title' => $this->title(),
            'choice_title' => $this->choiceTitle(),
            'content' => $this->content(),
            'list_links' => $this->listLinks(),
            'link' => $this->link(),
            'choice_side' => $this->choiceSide(),
            'choice_text' => $this->choiceText(),
            'choice_height' => $this->choiceHeight(),
        ];
    }

    public function image()
    {
        return get_field('image');
    }

    public function double_image()
    {
        return get_field('double_image');
    }

    public function title(): ?string
    {
        return get_field('title');
    }

    public function choiceTitle(): ?bool
    {
        return get_field('choice_title');
    }

    public function listLinks()
    {
        return get_field('repeater_link');
    }

    public function content(): ?string
    {
        return get_field('content');
    }

    public function link()
    {
        return get_field('link');
    }

    public function choiceSide(): ?bool
    {
        return get_field('choice_side');
    }

    public function choiceText(): ?bool
    {
        return get_field('choice_text');
    }

    public function choiceHeight(): ?bool
    {
        return get_field('choice_height');
    }
}
