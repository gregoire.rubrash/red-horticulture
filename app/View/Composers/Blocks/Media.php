<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class Media extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.media',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'choice' => $this->choice(),
            'image' => $this->image(),
            'video' => $this->video(),
            'choice_size' => $this->choiceSize(),
        ];
    }

    public function choice(): ?bool
    {
        return get_field('choice');
    }

    public function image()
    {
        return get_field('image');
    }

    public function video(): ?array
    {
        return get_field('video');
    }

    public function choiceSize()
    {
        return get_field('choice_size');
    }
}
