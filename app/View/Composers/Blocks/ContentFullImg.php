<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class ContentFullImg extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.content-full-img',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'image' => $this->image(),
            'title' => $this->title(),
            'content' => $this->content(),
            'link' => $this->link(),
            'alignRight' => $this->alignRight(),
            'hasBg' => $this->hasBg(),
        ];
    }

    public function image(): mixed
    {
        $image = get_field('image');
        if (empty($image)) {
            return null;
        }

        return (object) [
            'src' => $image['url'],
            'alt' => $image['alt'],
            'sizes' => $image['sizes'],
        ];
    }

    public function title(): string
    {
        return get_field('title');
    }

    public function content(): mixed
    {
        return get_field('content');
    }

    public function link(): mixed
    {
        return get_field('link');
    }

    public function alignRight()
    {
        return get_field('alignRight');
    }

    public function hasBg()
    {
        return get_field('has_bg');
    }
}
