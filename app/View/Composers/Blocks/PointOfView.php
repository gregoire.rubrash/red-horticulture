<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class PointOfView extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.point-of-view',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'suptitle' => $this->suptitle(),
            'content' => $this->content(),
            'author' => $this->author(),
            'position' => $this->position(),
        ];
    }

    public function suptitle(): ?string
    {
        return get_field('suptitle');
    }

    public function content(): ?string
    {
        return get_field('content');
    }

    public function author(): ?string
    {
        return get_field('author');
    }

    public function position(): ?string
    {
        return get_field('position');
    }
}
