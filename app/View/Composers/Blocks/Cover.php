<?php

namespace App\View\Composers\Blocks;

use Roots\Acorn\View\Composer;

class Cover extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'blocks.cover',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'choice' => $this->choice(),
            'image' => $this->image(),
            'title_img' => $this->titleImg(),
            'video' => $this->video(),
            'title' => $this->title(),
            'content' => $this->content(),
        ];
    }

    public function choice(): ?bool
    {
        return get_field('choice');
    }

    public function image()
    {
        return get_field('image');
    }

    public function titleImg(): ?string
    {
        return get_field('title_img');
    }

    public function video(): ?array
    {
        return get_field('video');
    }

    public function title(): ?string
    {
        return get_field('title');
    }

    public function content(): ?string
    {
        return get_field('content');
    }
}
