<?php

namespace App\View\Composers\Singles;

use App\Helpers\Thumbnail;
use Roots\Acorn\View\Composer;

class SingleEvent extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'layouts.singles.evenement',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        global $post;

        return [
            'title' => $this->title(),
            'thumbnail' => $this->thumbnail(),
            'excerpt' => $this->excerpt(),
            'date' => $this->date(),
        ];
    }

    public function title() {
        return get_the_title();
    }

    public function thumbnail() {
        return Thumbnail::get(get_the_ID(), 'full');
    }

    public function excerpt() {
        return get_the_excerpt();
    }

    public function date() {
        return get_the_date('j F Y');
    }
}
