<?php

namespace App\View\Composers\Singles;

use App\Helpers\Thumbnail;
use Roots\Acorn\View\Composer;

class SingleCareer extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'layouts.singles.carriere',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        global $post;

        return [
            'title' => $this->title(),
            'city' => $this->city(),
            'when' => $this->when(),
            'contracts' => $this->contracts($post->ID),
        ];
    }

    public function title() {
        return get_the_title();
    }

    public function city() {
        return get_field('city');
    }

    public function when() {
        return get_field('when');
    }

    public function contracts($postID) {
        return get_terms([
            'taxonomy' => 'type-de-job',
            'object_ids' => $postID,
            'fields' => 'names',
        ]);
    }
}
