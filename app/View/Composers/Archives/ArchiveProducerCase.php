<?php

namespace App\View\Composers\Archives;

use App\Helpers\Query;
use App\Helpers\Thumbnail;
use Roots\Acorn\View\Composer;

class ArchiveProducerCase extends Composer
{
    const POST_TYPE = 'cas-producteur';

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'layouts.archives.cas-producteur',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        $highlightedPost = $this->getPostData($this->highlightedPost());
        $posts = $this->getPostsData($highlightedPost->ID ?? null);

        return [
            'highlightedPost' => $highlightedPost,
            'posts' => $posts,
            'query' => Query::get(),
        ];
    }

    private function getPostData($item, ?bool $isHighlighted = false)
    {
        if (empty($item)) {
            return null;
        }

        return (object) [
            'ID' => $item->ID,
            'title' => $item->post_title,
            'hasThumbnail' => has_post_thumbnail($item->ID),
            'thumbnail' => $isHighlighted ? Thumbnail::get($item->ID, 'full') : Thumbnail::get($item->ID),
            'excerpt' => wp_trim_words(get_the_excerpt($item->ID), 6, '...'),
            'link' => get_permalink($item->ID),
        ];
    }

    private function highlightedPost()
    {
        $query = new \WP_Query([
            'post_type' => self::POST_TYPE,
            'posts_per_page' => 1,
            'orderby' => 'date',
            'order' => 'DESC',
        ]);

        $queryPosts = collect($query->posts);

        return $queryPosts->isEmpty() ? [] : $queryPosts->first();
    }

    private function getPostsData($highlightedPostID)
    {
        $query = new \WP_Query([
            'post_type' => self::POST_TYPE,
            'posts_per_page' => -1,
            's' => Query::get(),
            'post__not_in' => [$highlightedPostID],
        ]);

        $queryPosts = collect($query->posts);

        return $queryPosts->isEmpty() ? [] : $queryPosts->map(function ($item) {
            return $this->getPostData($item);
        })->filter()->all();
    }
}
