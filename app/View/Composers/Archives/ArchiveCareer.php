<?php

namespace App\View\Composers\Archives;

use Roots\Acorn\View\Composer;

class ArchiveCareer extends Composer
{
    const POST_TYPE = 'carriere';

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'layouts.archives.carriere',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'jobs' => $this->getPostsData(),
        ];
    }

    private function getPostData($item)
    {
        if (empty($item)) {
            return null;
        }

        return (object) [
            'ID' => $item->ID,
            'title' => $item->post_title,
            'city' =>  get_field('city', $item->ID),
            'contracts' => get_terms([
                'taxonomy' => 'type-de-job',
                'object_ids' => $item->ID,
                'fields' => 'names',
            ]),
            'link' => get_permalink($item->ID),
        ];
    }

    private function getPostsData()
    {
        $query = new \WP_Query([
            'post_type' => self::POST_TYPE,
            'posts_per_page' => -1,
        ]);

        $queryPosts = collect($query->posts);

        return $queryPosts->isEmpty() ? [] : $queryPosts->map(function ($item) {
            return $this->getPostData($item);
        })->filter()->all();
    }
}
