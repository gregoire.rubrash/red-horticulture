<?php

namespace App\View\Composers\Archives;

use App\Helpers\Thumbnail;
use Roots\Acorn\View\Composer;

class ArchivePost extends Composer
{
    const POST_TYPE = 'post';

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'layouts.archives.post',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        $highlightedPost = $this->getPostData($this->highlightedPost(), true);
        $posts = $this->getPostsData($highlightedPost->ID ?? null);

        return [
            'highlightedPost' => $highlightedPost,
            'posts' => $posts,
        ];
    }

    private function getPostData($item, ?bool $isHighlighted = false)
    {
        if (empty($item)) {
            return null;
        }

        return (object) [
            'ID' => $item->ID,
            'title' => $item->post_title,
            'hasThumbnail' => has_post_thumbnail($item->ID),
            'thumbnail' => $isHighlighted ? Thumbnail::get($item->ID, 'full') : Thumbnail::get($item->ID),
            'categories' =>  get_the_category($item->ID),
            'excerpt' => wp_trim_words(get_the_excerpt($item->ID), 6, '...'),
            'link' => get_permalink($item->ID),
        ];
    }

    private function highlightedPost()
    {
        $query = new \WP_Query([
            'post_type' => self::POST_TYPE,
            'posts_per_page' => 1,
            'orderby' => 'date',
            'order' => 'DESC',
        ]);

        $queryPosts = collect($query->posts);

        return $queryPosts->isEmpty() ? [] : $queryPosts->first();
    }

    private function getPostsData($highlightedPostID)
    {
        $query = new \WP_Query([
            'post_type' => self::POST_TYPE,
            'posts_per_page' => -1,
            's' => get_search_query(),
            'post__not_in' => [$highlightedPostID],
        ]);

        $queryPosts = collect($query->posts);

        return $queryPosts->isEmpty() ? [] : $queryPosts->map(function ($item) {
            return $this->getPostData($item);
        })->filter()->all();
    }
}
