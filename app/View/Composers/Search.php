<?php

namespace App\View\Composers;

use Roots\Acorn\View\Composer;

class Search extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'search',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'posts' => $this->posts(),
        ];
    }

    private function posts(): array
    {
        $query = new \WP_Query([
            'post_type' => 'post',
            'posts_per_page' => -1,
            's' => get_search_query(),
        ]);

        $queryPosts = collect($query->posts);

        if ($queryPosts->isEmpty()) {
            return null;
        }

        return $queryPosts->map(function ($item) {
            if (empty($item)) {
                return null;
            }

            $hasThumbnail = has_post_thumbnail($item->ID);
            $thumbnail = null;
            if ($hasThumbnail) {
                $thumbnailID = get_post_thumbnail_id($item->ID);
                $thumbnailAttachment = wp_get_attachment_image_src($thumbnailID);
                $thumbnail = (object) [
                    'src' => $thumbnailAttachment[0],
                    'alt' => get_post_meta($thumbnailID, '_wp_attachment_image_alt', true),
                    'sizes' => [],
                ];
            }

            return (object) [
                'ID' => $item->ID,
                'title' => $item->post_title,
                'hasThumbnail' => $hasThumbnail,
                'thumbnail' => $thumbnail,
                'excerpt' => wp_trim_words(get_the_excerpt($item->ID), 6, '...'),
                'link' => get_permalink($item->ID),
            ];
        })->filter()->all();
    }
}
