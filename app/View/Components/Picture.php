<?php

namespace App\View\Components;

use Roots\Acorn\View\Component;
use Illuminate\Support\Str;

/**
 * Picture component
 * @author Grégoire Ciles <bonjour@gregoireciles.fr>
 */
class Picture extends Component
{
    /**
     * The picture url
     *
     * @var string
     */
    public string $url;

    /**
     * The picture alt
     *
     * @var string
     */
    public string $alt;

    /**
     * The picture class
     *
     * @var ?string
     */
    public ?string $pictureClass;

    /**
     * The image class
     *
     * @var ?string
     */
    public ?string $imageClass;

    /**
     * Create the component instance.
     *
     * @param string $url
     * @param string $alt
     * @param array $sizes
     *
     * @return void
     */
    public function __construct(
        string $url,
        string $alt,
        ?string $pictureClass = null,
        ?string $imageClass = null
    ) {
        $this->url = $url;
        $this->alt = $alt;

        $this->pictureClass = $pictureClass;
        $this->imageClass = $imageClass;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return $this->view('components.picture');
    }
}
