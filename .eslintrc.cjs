module.exports = {
  root: true,
  extends: [`@roots/eslint-config/sage`, `plugin:react/jsx-runtime`],
  ignorePatterns: [
    `!.*.js`,
    `!.*.ts`,
    `!.*.tsx`,
    `node_modules`,
    `public`,
    `.budfiles`,
  ],
};
