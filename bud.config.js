/**
 * Build configuration
 *
 * @see {@link https://roots.io/docs/sage/ sage documentation}
 * @see {@link https://bud.js.org/guides/configure/ bud.js configuration guide}
 *
 * @typedef {import('@roots/bud').Bud} Bud
 * @param {Bud} app
 */
export default async (app) => {
  app
    .proxy(`http://red.local`)
    .serve(`http://localhost:4200`)
    .watch([app.path(`resources/views`), app.path(`app`)])

    .entry({
      app: ['@scripts/app', '@styles/app'],
      editor: ['@scripts/editor', '@styles/editor'],
    })
    .assets(['images'])

    .setPublicPath('/wp-content/themes/red-horticulture/public/')
    .experiments(`topLevelAwait`, true)

    .wpjson.settings({
      color: {
        custom: false,
        customDuotone: false,
        customGradient: false,
        defaultDuotone: false,
        defaultGradients: false,
        defaultPalette: false,
        duotone: [],
      },
      custom: {
        spacing: {},
        typography: {
          'font-size': {},
          'line-height': {},
        },
      },
      spacing: {
        padding: true,
        units: ['px', '%', 'em', 'rem', 'vw', 'vh'],
      },
      typography: {
        customFontSize: false,
      },
    })
    .useTailwindColors()
    .useTailwindFontFamily()
    .useTailwindFontSize()
    .enable();

  /**
   * Eslint config (when sed by the child compiler)
   */
  app
    .when(`eslint` in app, ({ eslint }) =>
      eslint
        .extends([`@roots/eslint-config/sage`, `plugin:react/jsx-runtime`])
        .setFix(true)
        .setFailOnWarning(app.isProduction),
    )

    /**
     * Image minification config
     */
    .when(`imagemin` in app, ({ imagemin }) =>
      imagemin.encode(`jpeg`, { mozjpeg: true, quality: 70 }),
    );
};
